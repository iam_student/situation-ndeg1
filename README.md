
## HNT

Humanist Network souhaite développer une application Windows afin de gérer les missions humanitaires dans le monde entier.
Cette application devra être supportée par un serveur W2012Server, sur lequel vous devrez implanter SQL Server pour gérer la base de données nécessaire.

---

## Partie Développement

Cette application sera développée en Visual Basic en utilisant Visual Studio 2013 et la programmation orientée objet.
La base de données sera hébergée sur le SGBDR suivant : SQL Server de Microsoft 2016.
Une mission humanitaire est créée à la suite d’une idée lancée par un membre de HNT, mais elle peut aussi être une action déjà menée et que les membres de HNT décident de refaire, car ils estiment qu’elle a été bénéfique. Outre le nom de l’action, un descriptif de celle-ci sera nécessaire ainsi que sa date de création, une date de début de projet et une date de fin de projet. La mission a lieu dans un pays dont on veut connaître certes le nom mais également le niveau de risque. Il existe 5 niveaux de risque selon la dangerosité du pays : « Aucun risque », « Risque faible », « Risque moyen », « Risque élevé », « Risque très élevé ».
Les personnels de HNT participant aux actions sont appelés « Bénévoles » et seront identifiés par un numéro qui permettra de connaitre leur nom, prénom, adresse, leur date d’entrée en tant que Bénévole et éventuellement leur date de sortie mais surtout leur GSM. Il est intéressant de connaitre aussi les « Donateurs » de HNT (personnes qui font un dont pour ces actions humanitaires par exemples sans distinction d’action). Ces Donateurs seront identifiés par un numéro qui permettra de connaitre leur nom, prénom, adresse et surtout le(s) montant(s) de leur(s) don(s) ainsi que les dates des dons. Un Bénévole peut être un Donateur.
Afin d’envoyer les bons bénévoles sur les bonnes missions il est important de connaître les spécialités de chaque bénévole. Il faut également identifier quel sera le rôle de chacun des bénévoles sur les missions sachant qu’un rôle fait appel à une spécialité. Il est important que le bénévole envoyé sur une mission avec un rôle précis reposant sur une spécialité soit compétent dans cette spécialité.
En fonction du pays il est important de connaître quels sont les vaccins nécessaires et la description de ce vaccin.
