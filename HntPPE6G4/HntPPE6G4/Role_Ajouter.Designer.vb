﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Role_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Role_Ajouter))
        Me.cbcodespec = New System.Windows.Forms.ComboBox()
        Me.lcodespec = New System.Windows.Forms.Label()
        Me.tblibrole = New System.Windows.Forms.TextBox()
        Me.llibrole = New System.Windows.Forms.Label()
        Me.bajout = New System.Windows.Forms.Button()
        Me.lroleajout = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbcodespec
        '
        Me.cbcodespec.FormattingEnabled = True
        Me.cbcodespec.Location = New System.Drawing.Point(136, 99)
        Me.cbcodespec.Name = "cbcodespec"
        Me.cbcodespec.Size = New System.Drawing.Size(121, 21)
        Me.cbcodespec.TabIndex = 35
        '
        'lcodespec
        '
        Me.lcodespec.AutoSize = True
        Me.lcodespec.BackColor = System.Drawing.Color.White
        Me.lcodespec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lcodespec.Location = New System.Drawing.Point(12, 103)
        Me.lcodespec.Name = "lcodespec"
        Me.lcodespec.Size = New System.Drawing.Size(114, 17)
        Me.lcodespec.TabIndex = 34
        Me.lcodespec.Text = "Code Spécialité :"
        '
        'tblibrole
        '
        Me.tblibrole.Location = New System.Drawing.Point(133, 63)
        Me.tblibrole.Name = "tblibrole"
        Me.tblibrole.Size = New System.Drawing.Size(100, 20)
        Me.tblibrole.TabIndex = 33
        '
        'llibrole
        '
        Me.llibrole.AutoSize = True
        Me.llibrole.BackColor = System.Drawing.Color.White
        Me.llibrole.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llibrole.Location = New System.Drawing.Point(12, 63)
        Me.llibrole.Name = "llibrole"
        Me.llibrole.Size = New System.Drawing.Size(57, 17)
        Me.llibrole.TabIndex = 32
        Me.llibrole.Text = "Libellé :"
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(136, 140)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(118, 30)
        Me.bajout.TabIndex = 31
        Me.bajout.Text = "Ajouter"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'lroleajout
        '
        Me.lroleajout.AutoSize = True
        Me.lroleajout.BackColor = System.Drawing.Color.White
        Me.lroleajout.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lroleajout.Location = New System.Drawing.Point(12, 9)
        Me.lroleajout.Name = "lroleajout"
        Me.lroleajout.Size = New System.Drawing.Size(200, 31)
        Me.lroleajout.TabIndex = 28
        Me.lroleajout.Text = "Ajouter un Rôle"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(12, 176)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Role_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(272, 211)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.cbcodespec)
        Me.Controls.Add(Me.lcodespec)
        Me.Controls.Add(Me.tblibrole)
        Me.Controls.Add(Me.llibrole)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.lroleajout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Role_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Role_Ajouter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbcodespec As System.Windows.Forms.ComboBox
    Friend WithEvents lcodespec As System.Windows.Forms.Label
    Friend WithEvents tblibrole As System.Windows.Forms.TextBox
    Friend WithEvents llibrole As System.Windows.Forms.Label
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents lroleajout As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class
