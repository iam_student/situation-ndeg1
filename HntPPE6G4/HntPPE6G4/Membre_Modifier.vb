﻿Public Class Membre_modifier
    Dim maconnexion As New Connexion
    Private Sub bmodif_Click(sender As Object, e As EventArgs) Handles bmodif.Click
        maconnexion.modifier_benevole(id:=tbid.Text, nom:=tbnommembre.Text, prenom:=tbprenommembre.Text, adr:=tbadresse.Text, gsm:=tbgsm.Text, datefin:=datesortie.Value.ToShortDateString())
        Me.Hide()
        Membre.Show()
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Membre.Show()
    End Sub

    Private Sub tbgsm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbgsm.KeyPress

        If IsNumeric(e.KeyChar) Then

            e.Handled = False

        Else

            e.Handled = True

        End If

    End Sub
End Class