﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pays
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Pays))
        Me.bnouveau = New System.Windows.Forms.Button()
        Me.bsupprimer = New System.Windows.Forms.Button()
        Me.bmodifier = New System.Windows.Forms.Button()
        Me.lpays = New System.Windows.Forms.Label()
        Me.dgvpays = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nom_Pays = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Risque = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bmenu = New System.Windows.Forms.Button()
        Me.bajoutvac = New System.Windows.Forms.Button()
        Me.lvaccin = New System.Windows.Forms.Label()
        Me.lvvaccin = New System.Windows.Forms.ListView()
        Me.ID_PAYS = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ID_Vaccin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Desc_Vaccin = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btsupmembre = New System.Windows.Forms.Button()
        CType(Me.dgvpays, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnouveau
        '
        Me.bnouveau.BackColor = System.Drawing.Color.White
        Me.bnouveau.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouveau.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouveau.Location = New System.Drawing.Point(12, 265)
        Me.bnouveau.Name = "bnouveau"
        Me.bnouveau.Size = New System.Drawing.Size(192, 45)
        Me.bnouveau.TabIndex = 9
        Me.bnouveau.Text = "NOUVEAU PAYS"
        Me.bnouveau.UseVisualStyleBackColor = False
        '
        'bsupprimer
        '
        Me.bsupprimer.BackColor = System.Drawing.Color.White
        Me.bsupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsupprimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsupprimer.Location = New System.Drawing.Point(334, 265)
        Me.bsupprimer.Name = "bsupprimer"
        Me.bsupprimer.Size = New System.Drawing.Size(141, 45)
        Me.bsupprimer.TabIndex = 8
        Me.bsupprimer.Text = "SUPPRIMER"
        Me.bsupprimer.UseVisualStyleBackColor = False
        '
        'bmodifier
        '
        Me.bmodifier.BackColor = System.Drawing.Color.White
        Me.bmodifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodifier.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmodifier.Location = New System.Drawing.Point(210, 265)
        Me.bmodifier.Name = "bmodifier"
        Me.bmodifier.Size = New System.Drawing.Size(118, 45)
        Me.bmodifier.TabIndex = 7
        Me.bmodifier.Text = "MODIFIER"
        Me.bmodifier.UseVisualStyleBackColor = False
        '
        'lpays
        '
        Me.lpays.AutoSize = True
        Me.lpays.BackColor = System.Drawing.Color.White
        Me.lpays.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lpays.Location = New System.Drawing.Point(12, 9)
        Me.lpays.Name = "lpays"
        Me.lpays.Size = New System.Drawing.Size(86, 31)
        Me.lpays.TabIndex = 6
        Me.lpays.Text = "PAYS"
        '
        'dgvpays
        '
        Me.dgvpays.AllowUserToAddRows = False
        Me.dgvpays.AllowUserToDeleteRows = False
        Me.dgvpays.AllowUserToResizeColumns = False
        Me.dgvpays.AllowUserToResizeRows = False
        Me.dgvpays.BackgroundColor = System.Drawing.Color.White
        Me.dgvpays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvpays.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Nom_Pays, Me.Risque})
        Me.dgvpays.Location = New System.Drawing.Point(12, 62)
        Me.dgvpays.Name = "dgvpays"
        Me.dgvpays.Size = New System.Drawing.Size(344, 184)
        Me.dgvpays.TabIndex = 5
        '
        'ID
        '
        Me.ID.Frozen = True
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'Nom_Pays
        '
        Me.Nom_Pays.Frozen = True
        Me.Nom_Pays.HeaderText = "Pays"
        Me.Nom_Pays.Name = "Nom_Pays"
        Me.Nom_Pays.ReadOnly = True
        '
        'Risque
        '
        Me.Risque.Frozen = True
        Me.Risque.HeaderText = "Risque"
        Me.Risque.Name = "Risque"
        Me.Risque.ReadOnly = True
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(715, 285)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(116, 47)
        Me.bmenu.TabIndex = 10
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'bajoutvac
        '
        Me.bajoutvac.BackColor = System.Drawing.Color.White
        Me.bajoutvac.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajoutvac.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bajoutvac.Location = New System.Drawing.Point(483, 265)
        Me.bajoutvac.Name = "bajoutvac"
        Me.bajoutvac.Size = New System.Drawing.Size(207, 45)
        Me.bajoutvac.TabIndex = 11
        Me.bajoutvac.Text = "AJOUTER VACCIN"
        Me.bajoutvac.UseVisualStyleBackColor = False
        '
        'lvaccin
        '
        Me.lvaccin.AutoSize = True
        Me.lvaccin.BackColor = System.Drawing.Color.White
        Me.lvaccin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvaccin.Location = New System.Drawing.Point(438, 42)
        Me.lvaccin.Name = "lvaccin"
        Me.lvaccin.Size = New System.Drawing.Size(57, 17)
        Me.lvaccin.TabIndex = 13
        Me.lvaccin.Text = "Vaccins"
        '
        'lvvaccin
        '
        Me.lvvaccin.BackColor = System.Drawing.Color.White
        Me.lvvaccin.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ID_PAYS, Me.ID_Vaccin, Me.Desc_Vaccin})
        Me.lvvaccin.FullRowSelect = True
        Me.lvvaccin.Location = New System.Drawing.Point(441, 62)
        Me.lvvaccin.Name = "lvvaccin"
        Me.lvvaccin.Size = New System.Drawing.Size(249, 184)
        Me.lvvaccin.TabIndex = 14
        Me.lvvaccin.UseCompatibleStateImageBehavior = False
        Me.lvvaccin.View = System.Windows.Forms.View.Details
        '
        'ID_PAYS
        '
        Me.ID_PAYS.Text = "ID Pays"
        Me.ID_PAYS.Width = 63
        '
        'ID_Vaccin
        '
        Me.ID_Vaccin.Text = "ID Vaccin"
        Me.ID_Vaccin.Width = 74
        '
        'Desc_Vaccin
        '
        Me.Desc_Vaccin.Text = "Description Vaccin"
        Me.Desc_Vaccin.Width = 107
        '
        'btsupmembre
        '
        Me.btsupmembre.BackColor = System.Drawing.Color.White
        Me.btsupmembre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btsupmembre.Location = New System.Drawing.Point(730, 108)
        Me.btsupmembre.Name = "btsupmembre"
        Me.btsupmembre.Size = New System.Drawing.Size(82, 68)
        Me.btsupmembre.TabIndex = 15
        Me.btsupmembre.Text = "SUPPRIMER UN VACCIN"
        Me.btsupmembre.UseVisualStyleBackColor = False
        '
        'Pays
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(843, 344)
        Me.Controls.Add(Me.btsupmembre)
        Me.Controls.Add(Me.lvvaccin)
        Me.Controls.Add(Me.lvaccin)
        Me.Controls.Add(Me.bajoutvac)
        Me.Controls.Add(Me.bmenu)
        Me.Controls.Add(Me.bnouveau)
        Me.Controls.Add(Me.bsupprimer)
        Me.Controls.Add(Me.bmodifier)
        Me.Controls.Add(Me.lpays)
        Me.Controls.Add(Me.dgvpays)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Pays"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pays"
        CType(Me.dgvpays, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnouveau As System.Windows.Forms.Button
    Friend WithEvents bsupprimer As System.Windows.Forms.Button
    Friend WithEvents bmodifier As System.Windows.Forms.Button
    Friend WithEvents lpays As System.Windows.Forms.Label
    Friend WithEvents dgvpays As System.Windows.Forms.DataGridView
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nom_Pays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Risque As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bajoutvac As System.Windows.Forms.Button
    Friend WithEvents lvaccin As System.Windows.Forms.Label
    Friend WithEvents lvvaccin As System.Windows.Forms.ListView
    Friend WithEvents ID_PAYS As System.Windows.Forms.ColumnHeader
    Friend WithEvents ID_Vaccin As System.Windows.Forms.ColumnHeader
    Friend WithEvents Desc_Vaccin As System.Windows.Forms.ColumnHeader
    Friend WithEvents btsupmembre As System.Windows.Forms.Button
End Class
