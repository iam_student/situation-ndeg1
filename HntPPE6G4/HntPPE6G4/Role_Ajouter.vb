﻿Public Class Role_Ajouter
    Dim maconnexion As New Connexion
    Dim idspe As String
    Private Sub Role_Ajouter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_role_spec()
        tblibrole.Text = ""
    End Sub
    Private Sub bajout_Click(sender As Object, e As EventArgs) Handles bajout.Click
        Dim libelle As String
        libelle = tblibrole.Text
        idspe = cbcodespec.SelectedItem
        maconnexion.ajouter_role(codespe:=idspe, libelle:=libelle)
        Me.Close()
        Role.Show()
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Role.Show()
    End Sub
End Class