﻿Public Class Vaccin
    Dim maconnexion As New Connexion
    Private Sub Vaccin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_vaccin()
        bmodifier.Visible = False
        bsupprimer.Visible = False
    End Sub

    Private Sub bmodifier_Click(sender As Object, e As EventArgs) Handles bmodifier.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        With dgvvaccin
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            Vaccin_Modifier.tbdescrip.Text = ValLigne
            Vaccin_Modifier.tbid.Text = IdLigne
        End With
        Me.Close()
        Vaccin_Modifier.Show()
    End Sub

    Private Sub bnouveau_Click(sender As Object, e As EventArgs) Handles bnouveau.Click
        Me.Close()
        Vaccin_Ajouter.Show()
    End Sub

    Private Sub bmenu_Click(sender As Object, e As EventArgs) Handles bmenu.Click
        Me.Close()
        Menu_app.Show()
    End Sub
    Private Sub bsupprimer_Click(sender As Object, e As EventArgs) Handles bsupprimer.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        With dgvvaccin
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            If MsgBox("Etes vous sur de vouloir supprimer le vaccin : " + IdLigne + " ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maconnexion.supprimer_vaccin(ValLigne)
            End If
        End With
        Me.Close()
        Vaccin_Chargement.Show()
    End Sub
    Private Sub dgvvaccin_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvvaccin.CellMouseClick
        bmodifier.Visible = True
        bsupprimer.Visible = True
    End Sub
End Class