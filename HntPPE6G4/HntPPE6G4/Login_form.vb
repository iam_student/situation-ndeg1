﻿Public Class Login_form
    Dim maConnexion As New Connexion()


    Private Sub bquitter_Click(sender As Object, e As EventArgs) Handles bquitter.Click
        If MsgBox("Quitter l'application ?", vbQuestion + vbYesNo, "Quitter") = vbYes Then
            Me.Close()
        End If
    End Sub

    Private Sub bconnecter_Click(sender As Object, e As EventArgs) Handles bconnecter.Click
        If tblogin.Text <> "" And tbmdp.Text <> "" Then
            maConnexion.login(tblogin.Text, tbmdp.Text)
        End If

    End Sub

    Private Sub Login_form_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maConnexion.remplir_collectiondonateur()
        maConnexion.remplir_collectiondon()
    End Sub
End Class
