﻿Public Class Pays_Modifier
    Dim maconnexion As New Connexion
    Private Sub bmodif_Click(sender As Object, e As EventArgs) Handles bmodif.Click
        If rbaucun.Checked = True Then
            maconnexion.modifier_pays(tbid.Text, tbnom.Text, rbaucun.Text)
        End If
        If rbfaible.Checked = True Then
            maconnexion.modifier_pays(tbid.Text, tbnom.Text, rbfaible.Text)
        End If
        If rbmoyen.Checked = True Then
            maconnexion.modifier_pays(tbid.Text, tbnom.Text, rbmoyen.Text)
        End If
        If rbeleve.Checked = True Then
            maconnexion.modifier_pays(tbid.Text, tbnom.Text, rbeleve.Text)
        End If
        If rbtreseleve.Checked = True Then
            maconnexion.modifier_pays(tbid.Text, tbnom.Text, rbtreseleve.Text)
        End If
        Me.Hide()
        Pays.Show()
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Pays.Show()
    End Sub
End Class