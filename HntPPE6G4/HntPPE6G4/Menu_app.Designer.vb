﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Menu_app
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Menu_app))
        Me.lmenu = New System.Windows.Forms.Label()
        Me.bmission = New System.Windows.Forms.Button()
        Me.bmembres = New System.Windows.Forms.Button()
        Me.bdons = New System.Windows.Forms.Button()
        Me.bparam = New System.Windows.Forms.Button()
        Me.bpays = New System.Windows.Forms.Button()
        Me.bvaccin = New System.Windows.Forms.Button()
        Me.brole = New System.Windows.Forms.Button()
        Me.bspec = New System.Windows.Forms.Button()
        Me.bdeco = New System.Windows.Forms.Button()
        Me.bquitter = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lmenu
        '
        Me.lmenu.AutoSize = True
        Me.lmenu.BackColor = System.Drawing.Color.Transparent
        Me.lmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.lmenu.ForeColor = System.Drawing.Color.White
        Me.lmenu.Location = New System.Drawing.Point(12, 9)
        Me.lmenu.Name = "lmenu"
        Me.lmenu.Size = New System.Drawing.Size(175, 25)
        Me.lmenu.TabIndex = 0
        Me.lmenu.Text = "Choisir une option:"
        '
        'bmission
        '
        Me.bmission.BackColor = System.Drawing.Color.White
        Me.bmission.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmission.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.bmission.Location = New System.Drawing.Point(12, 45)
        Me.bmission.Name = "bmission"
        Me.bmission.Size = New System.Drawing.Size(115, 36)
        Me.bmission.TabIndex = 1
        Me.bmission.Text = "MISSION"
        Me.bmission.UseVisualStyleBackColor = False
        '
        'bmembres
        '
        Me.bmembres.BackColor = System.Drawing.Color.White
        Me.bmembres.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmembres.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.bmembres.Location = New System.Drawing.Point(133, 45)
        Me.bmembres.Name = "bmembres"
        Me.bmembres.Size = New System.Drawing.Size(128, 36)
        Me.bmembres.TabIndex = 2
        Me.bmembres.Text = "MEMBRES"
        Me.bmembres.UseVisualStyleBackColor = False
        '
        'bdons
        '
        Me.bdons.BackColor = System.Drawing.Color.White
        Me.bdons.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bdons.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.bdons.Location = New System.Drawing.Point(267, 45)
        Me.bdons.Name = "bdons"
        Me.bdons.Size = New System.Drawing.Size(95, 36)
        Me.bdons.TabIndex = 3
        Me.bdons.Text = "DONS"
        Me.bdons.UseVisualStyleBackColor = False
        '
        'bparam
        '
        Me.bparam.BackColor = System.Drawing.Color.White
        Me.bparam.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bparam.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!)
        Me.bparam.Location = New System.Drawing.Point(368, 45)
        Me.bparam.Name = "bparam"
        Me.bparam.Size = New System.Drawing.Size(149, 36)
        Me.bparam.TabIndex = 4
        Me.bparam.Text = "PARAMETRE"
        Me.bparam.UseVisualStyleBackColor = False
        '
        'bpays
        '
        Me.bpays.BackColor = System.Drawing.Color.White
        Me.bpays.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bpays.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.bpays.Location = New System.Drawing.Point(396, 87)
        Me.bpays.Name = "bpays"
        Me.bpays.Size = New System.Drawing.Size(95, 49)
        Me.bpays.TabIndex = 5
        Me.bpays.Text = "PAYS"
        Me.bpays.UseVisualStyleBackColor = False
        '
        'bvaccin
        '
        Me.bvaccin.BackColor = System.Drawing.Color.White
        Me.bvaccin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bvaccin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.bvaccin.Location = New System.Drawing.Point(396, 142)
        Me.bvaccin.Name = "bvaccin"
        Me.bvaccin.Size = New System.Drawing.Size(95, 49)
        Me.bvaccin.TabIndex = 6
        Me.bvaccin.Text = "VACCIN"
        Me.bvaccin.UseVisualStyleBackColor = False
        '
        'brole
        '
        Me.brole.BackColor = System.Drawing.Color.White
        Me.brole.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.brole.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.brole.Location = New System.Drawing.Point(396, 197)
        Me.brole.Name = "brole"
        Me.brole.Size = New System.Drawing.Size(95, 44)
        Me.brole.TabIndex = 7
        Me.brole.Text = "RÔLE"
        Me.brole.UseVisualStyleBackColor = False
        '
        'bspec
        '
        Me.bspec.BackColor = System.Drawing.Color.White
        Me.bspec.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bspec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.bspec.Location = New System.Drawing.Point(396, 247)
        Me.bspec.Name = "bspec"
        Me.bspec.Size = New System.Drawing.Size(95, 42)
        Me.bspec.TabIndex = 8
        Me.bspec.Text = "SPECIALITE"
        Me.bspec.UseVisualStyleBackColor = False
        '
        'bdeco
        '
        Me.bdeco.BackColor = System.Drawing.Color.White
        Me.bdeco.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bdeco.Location = New System.Drawing.Point(14, 313)
        Me.bdeco.Name = "bdeco"
        Me.bdeco.Size = New System.Drawing.Size(93, 23)
        Me.bdeco.TabIndex = 9
        Me.bdeco.Text = "Se déconnecter"
        Me.bdeco.UseVisualStyleBackColor = False
        '
        'bquitter
        '
        Me.bquitter.BackColor = System.Drawing.Color.White
        Me.bquitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bquitter.Location = New System.Drawing.Point(426, 323)
        Me.bquitter.Name = "bquitter"
        Me.bquitter.Size = New System.Drawing.Size(93, 23)
        Me.bquitter.TabIndex = 10
        Me.bquitter.Text = "Quitter"
        Me.bquitter.UseVisualStyleBackColor = False
        '
        'Menu_app
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(560, 358)
        Me.Controls.Add(Me.bquitter)
        Me.Controls.Add(Me.bdeco)
        Me.Controls.Add(Me.bspec)
        Me.Controls.Add(Me.brole)
        Me.Controls.Add(Me.bvaccin)
        Me.Controls.Add(Me.bpays)
        Me.Controls.Add(Me.bparam)
        Me.Controls.Add(Me.bdons)
        Me.Controls.Add(Me.bmembres)
        Me.Controls.Add(Me.bmission)
        Me.Controls.Add(Me.lmenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Menu_app"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Menu"
        Me.TopMost = True
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lmenu As System.Windows.Forms.Label
    Friend WithEvents bmission As System.Windows.Forms.Button
    Friend WithEvents bmembres As System.Windows.Forms.Button
    Friend WithEvents bdons As System.Windows.Forms.Button
    Friend WithEvents bparam As System.Windows.Forms.Button
    Friend WithEvents bpays As System.Windows.Forms.Button
    Friend WithEvents bvaccin As System.Windows.Forms.Button
    Friend WithEvents brole As System.Windows.Forms.Button
    Friend WithEvents bspec As System.Windows.Forms.Button
    Friend WithEvents bdeco As System.Windows.Forms.Button
    Friend WithEvents bquitter As System.Windows.Forms.Button
End Class
