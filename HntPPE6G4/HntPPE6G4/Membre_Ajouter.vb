﻿Public Class Membre_Ajouter
    Dim maconnexion As New Connexion
   
    Private Sub bajout_Click(sender As Object, e As EventArgs) Handles bajout.Click
        Dim nom = tbnommembre.Text
        Dim adresse = tbadresse.Text
        Dim GSM = tbgsm.Text
        Dim prenom = tbprenommembre.Text
        Dim datEntre = dateentre.Value.ToShortDateString()
        Dim datSortie = datesortie.Value.ToShortDateString()
        If cbbenevole.Checked = False And cbdon.Checked = True Then
            maconnexion.ajouter_donateur(nom:=nom, adresse:=adresse, GSM:=GSM, prenom:=prenom)
        End If
        If cbdon.Checked = False And cbbenevole.Checked = True Then
            maconnexion.ajouter_benevole(nom:=nom, prenom:=prenom, adresse:=adresse, GSM:=GSM, dateentre:=datEntre, datesortie:=datSortie)
            maconnexion.ajouter_spe_benevole(codespe:=cbspe.SelectedItem)
        End If
        If cbbenevole.Checked = True And cbdon.Checked = True Then
            maconnexion.ajouter_bene_don(nom:=nom, prenom:=prenom, adresse:=adresse, GSM:=GSM, dateentre:=datEntre, datesortie:=datSortie)
            maconnexion.ajouter_spe_benevole(codespe:=cbspe.SelectedItem)
        End If
        Me.Close()
        Membre_Chargement.Show()
    End Sub

    Private Sub Membre_Ajouter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbbenevole.Checked = True
        ldateentre.Visible = True
        ldatesortie.Visible = True
        dateentre.Visible = True
        datesortie.Visible = True
        tbnommembre.Text = ""
        tbadresse.Text = ""
        tbgsm.Text = ""
        tbprenommembre.Text = ""
        maconnexion.remplir_spe_benevole()
    End Sub

    Private Sub cbbenevole_CheckedChanged(sender As Object, e As EventArgs) Handles cbbenevole.CheckedChanged
        If cbdon.Checked = False Then
            cbbenevole.Checked = True
        End If
        If cbbenevole.Checked = True Then
            ldateentre.Visible = True
            ldatesortie.Visible = True
            lspe.Visible = True
            cbspe.Visible = True
            dateentre.Visible = True
            datesortie.Visible = True
        Else
            lspe.Visible = False
            cbspe.Visible = False
            ldateentre.Visible = False
            ldatesortie.Visible = False
            dateentre.Visible = False
            datesortie.Visible = False
        End If
    End Sub

    Private Sub cbdon_CheckedChanged(sender As Object, e As EventArgs) Handles cbdon.CheckedChanged
        If cbbenevole.Checked = False Then
            cbdon.Checked = True
        End If
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Membre.Show()
    End Sub

    Private Sub tbgsm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tbgsm.KeyPress

        If IsNumeric(e.KeyChar) Then

            e.Handled = False

        Else

            e.Handled = True

        End If

    End Sub
End Class