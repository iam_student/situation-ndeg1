﻿Public Class CDonnateur
    Public Shared m_numP As Integer
    Private NumP As Integer
    Private m_nomP As String
    Private m_prenomP As String
    Private m_adresseP As String
    Private m_GSMP As String


    Public Sub New(nomp, pnomp, adrp, GSMp)

        m_numP = m_numP + 1
        NumP = m_numP
        m_nomP = nomp
        m_prenomP = pnomp
        m_adresseP = adrp
        m_GSMP = GSMp

    End Sub
    Property getnumP
        Get
            Return NumP
        End Get
        Set(ByVal Value)
            NumP = Value
        End Set
    End Property
    Property getnomP
        Get
            Return m_nomP
        End Get
        Set(ByVal Value)
            m_nomP = Value
        End Set
    End Property
    Property getprenomP
        Get
            Return m_prenomP
        End Get
        Set(ByVal Value)
            m_prenomP = Value
        End Set
    End Property

    Property getadrP
        Get
            Return m_adresseP
        End Get
        Set(ByVal Value)
            m_adresseP = Value
        End Set
    End Property

    Property getGSMP
        Get
            Return m_GSMP
        End Get
        Set(ByVal Value)
            m_GSMP = Value
        End Set
    End Property
    
End Class
