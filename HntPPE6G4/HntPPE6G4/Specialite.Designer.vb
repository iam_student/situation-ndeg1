﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Specialite
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Specialite))
        Me.bnouvelle = New System.Windows.Forms.Button()
        Me.bsupprimer = New System.Windows.Forms.Button()
        Me.bmodifier = New System.Windows.Forms.Button()
        Me.lspec = New System.Windows.Forms.Label()
        Me.dgvspec = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Spécialité = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bmenu = New System.Windows.Forms.Button()
        CType(Me.dgvspec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnouvelle
        '
        Me.bnouvelle.BackColor = System.Drawing.Color.White
        Me.bnouvelle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouvelle.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouvelle.Location = New System.Drawing.Point(262, 59)
        Me.bnouvelle.Name = "bnouvelle"
        Me.bnouvelle.Size = New System.Drawing.Size(261, 58)
        Me.bnouvelle.TabIndex = 24
        Me.bnouvelle.Text = "NOUVELLE SPECIALITE"
        Me.bnouvelle.UseVisualStyleBackColor = False
        '
        'bsupprimer
        '
        Me.bsupprimer.BackColor = System.Drawing.Color.White
        Me.bsupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsupprimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsupprimer.Location = New System.Drawing.Point(262, 187)
        Me.bsupprimer.Name = "bsupprimer"
        Me.bsupprimer.Size = New System.Drawing.Size(261, 58)
        Me.bsupprimer.TabIndex = 23
        Me.bsupprimer.Text = "SUPPRIMER"
        Me.bsupprimer.UseVisualStyleBackColor = False
        '
        'bmodifier
        '
        Me.bmodifier.BackColor = System.Drawing.Color.White
        Me.bmodifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodifier.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmodifier.Location = New System.Drawing.Point(262, 123)
        Me.bmodifier.Name = "bmodifier"
        Me.bmodifier.Size = New System.Drawing.Size(261, 58)
        Me.bmodifier.TabIndex = 22
        Me.bmodifier.Text = "MODIFIER"
        Me.bmodifier.UseVisualStyleBackColor = False
        '
        'lspec
        '
        Me.lspec.AutoSize = True
        Me.lspec.BackColor = System.Drawing.Color.White
        Me.lspec.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lspec.Location = New System.Drawing.Point(12, 9)
        Me.lspec.Name = "lspec"
        Me.lspec.Size = New System.Drawing.Size(172, 31)
        Me.lspec.TabIndex = 21
        Me.lspec.Text = "SPECIALITE"
        '
        'dgvspec
        '
        Me.dgvspec.AllowUserToAddRows = False
        Me.dgvspec.AllowUserToDeleteRows = False
        Me.dgvspec.AllowUserToResizeColumns = False
        Me.dgvspec.AllowUserToResizeRows = False
        Me.dgvspec.BackgroundColor = System.Drawing.Color.White
        Me.dgvspec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvspec.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Spécialité})
        Me.dgvspec.Location = New System.Drawing.Point(12, 59)
        Me.dgvspec.Name = "dgvspec"
        Me.dgvspec.Size = New System.Drawing.Size(244, 184)
        Me.dgvspec.TabIndex = 20
        '
        'ID
        '
        Me.ID.Frozen = True
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'Spécialité
        '
        Me.Spécialité.Frozen = True
        Me.Spécialité.HeaderText = "Spécialité"
        Me.Spécialité.Name = "Spécialité"
        Me.Spécialité.ReadOnly = True
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(405, 251)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(151, 47)
        Me.bmenu.TabIndex = 25
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'Specialite
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(568, 310)
        Me.Controls.Add(Me.bmenu)
        Me.Controls.Add(Me.bnouvelle)
        Me.Controls.Add(Me.bsupprimer)
        Me.Controls.Add(Me.bmodifier)
        Me.Controls.Add(Me.lspec)
        Me.Controls.Add(Me.dgvspec)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Specialite"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Specialite"
        CType(Me.dgvspec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnouvelle As System.Windows.Forms.Button
    Friend WithEvents bsupprimer As System.Windows.Forms.Button
    Friend WithEvents bmodifier As System.Windows.Forms.Button
    Friend WithEvents lspec As System.Windows.Forms.Label
    Friend WithEvents dgvspec As System.Windows.Forms.DataGridView
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Spécialité As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
