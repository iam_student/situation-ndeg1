﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login_form
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Login_form))
        Me.lhnt = New System.Windows.Forms.Label()
        Me.llogin = New System.Windows.Forms.Label()
        Me.lmdp = New System.Windows.Forms.Label()
        Me.tbmdp = New System.Windows.Forms.TextBox()
        Me.tblogin = New System.Windows.Forms.TextBox()
        Me.bconnecter = New System.Windows.Forms.Button()
        Me.bquitter = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lhnt
        '
        Me.lhnt.AutoSize = True
        Me.lhnt.BackColor = System.Drawing.Color.Transparent
        Me.lhnt.Font = New System.Drawing.Font("Microsoft Sans Serif", 45.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lhnt.ForeColor = System.Drawing.Color.White
        Me.lhnt.Location = New System.Drawing.Point(12, 19)
        Me.lhnt.Name = "lhnt"
        Me.lhnt.Size = New System.Drawing.Size(153, 69)
        Me.lhnt.TabIndex = 0
        Me.lhnt.Text = "HNT"
        '
        'llogin
        '
        Me.llogin.AutoSize = True
        Me.llogin.BackColor = System.Drawing.Color.White
        Me.llogin.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llogin.ForeColor = System.Drawing.Color.Black
        Me.llogin.Location = New System.Drawing.Point(69, 191)
        Me.llogin.Name = "llogin"
        Me.llogin.Size = New System.Drawing.Size(71, 25)
        Me.llogin.TabIndex = 1
        Me.llogin.Text = "Login: "
        '
        'lmdp
        '
        Me.lmdp.AutoSize = True
        Me.lmdp.BackColor = System.Drawing.Color.White
        Me.lmdp.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmdp.Location = New System.Drawing.Point(41, 269)
        Me.lmdp.Name = "lmdp"
        Me.lmdp.Size = New System.Drawing.Size(141, 25)
        Me.lmdp.TabIndex = 2
        Me.lmdp.Text = "Mot de passe: "
        '
        'tbmdp
        '
        Me.tbmdp.Location = New System.Drawing.Point(217, 275)
        Me.tbmdp.Name = "tbmdp"
        Me.tbmdp.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.tbmdp.Size = New System.Drawing.Size(100, 20)
        Me.tbmdp.TabIndex = 3
        '
        'tblogin
        '
        Me.tblogin.Location = New System.Drawing.Point(217, 197)
        Me.tblogin.Name = "tblogin"
        Me.tblogin.Size = New System.Drawing.Size(100, 20)
        Me.tblogin.TabIndex = 4
        '
        'bconnecter
        '
        Me.bconnecter.BackColor = System.Drawing.Color.White
        Me.bconnecter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bconnecter.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bconnecter.Location = New System.Drawing.Point(203, 384)
        Me.bconnecter.Name = "bconnecter"
        Me.bconnecter.Size = New System.Drawing.Size(151, 47)
        Me.bconnecter.TabIndex = 5
        Me.bconnecter.Text = "Se connecter"
        Me.bconnecter.UseVisualStyleBackColor = False
        '
        'bquitter
        '
        Me.bquitter.BackColor = System.Drawing.Color.White
        Me.bquitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bquitter.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bquitter.Location = New System.Drawing.Point(318, 447)
        Me.bquitter.Name = "bquitter"
        Me.bquitter.Size = New System.Drawing.Size(151, 47)
        Me.bquitter.TabIndex = 6
        Me.bquitter.Text = "Quitter"
        Me.bquitter.UseVisualStyleBackColor = False
        '
        'Login_form
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PaleGreen
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(481, 506)
        Me.Controls.Add(Me.bquitter)
        Me.Controls.Add(Me.bconnecter)
        Me.Controls.Add(Me.tblogin)
        Me.Controls.Add(Me.tbmdp)
        Me.Controls.Add(Me.lmdp)
        Me.Controls.Add(Me.llogin)
        Me.Controls.Add(Me.lhnt)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Login_form"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login_Form"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lhnt As System.Windows.Forms.Label
    Friend WithEvents llogin As System.Windows.Forms.Label
    Friend WithEvents lmdp As System.Windows.Forms.Label
    Friend WithEvents tbmdp As System.Windows.Forms.TextBox
    Friend WithEvents tblogin As System.Windows.Forms.TextBox
    Friend WithEvents bconnecter As System.Windows.Forms.Button
    Friend WithEvents bquitter As System.Windows.Forms.Button

End Class
