﻿Public Class Membre
    Dim maconnexion As New Connexion
    Dim undonateur As New Connexion
    Private Sub Membre_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_benevole()
        undonateur.remplir_donateur()
        bmodifier.Visible = False
        bsupprimer.Visible = False
    End Sub
    Private Sub bmenu_Click(sender As Object, e As EventArgs) Handles bmenu.Click
        Me.Close()
        Menu_app.Show()
    End Sub

    Private Sub bmodifier_Click(sender As Object, e As EventArgs) Handles bmodifier.Click
        Dim NumLigne As Integer
        Dim IdLigne As String
        Dim NomM As String
        Dim PreM As String
        Dim AdrM As String
        Dim GSM As String
        With dgvbenevole
            NumLigne = .CurrentCell.RowIndex
            IdLigne = .Rows(NumLigne).Cells(0).Value
            NomM = .Rows(NumLigne).Cells(1).Value
            PreM = .Rows(NumLigne).Cells(2).Value
            AdrM = .Rows(NumLigne).Cells(3).Value
            GSM = .Rows(NumLigne).Cells(4).Value
            Membre_modifier.tbid.Text = IdLigne
            Membre_modifier.tbnommembre.Text = NomM
            Membre_modifier.tbprenommembre.Text = PreM
            Membre_modifier.tbadresse.Text = AdrM
            Membre_modifier.tbgsm.Text = GSM
        End With
        Me.Close()
        Membre_modifier.Show()
    End Sub

    Private Sub bnouveau_Click(sender As Object, e As EventArgs) Handles bnouveau.Click
        Me.Close()
        Membre_Ajouter.Show()
    End Sub

    Private Sub bsupprimer_Click(sender As Object, e As EventArgs) Handles bsupprimer.Click
        Dim NumLigne As Integer
        Dim ValLigne1 As String
        With dgvbenevole
            NumLigne = .CurrentCell.RowIndex
            ValLigne1 = .Rows(NumLigne).Cells(0).Value
            If MsgBox("Etes vous sur de vouloir supprimer le bénévole : " + ValLigne1 + " ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maconnexion.supprimer_benevole(ValLigne1)
            End If
        End With
        Me.Close()
        Membre_Chargement.Show()
    End Sub

    Private Sub dgvdonateur_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvdonateur.CellMouseClick
        bmodifier.Visible = True
        bsupprimer.Visible = True
    End Sub

    Private Sub gvbenevole_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvbenevole.CellMouseClick
        bmodifier.Visible = True
        bsupprimer.Visible = True
    End Sub

End Class