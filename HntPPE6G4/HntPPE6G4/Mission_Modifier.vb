﻿Public Class Mission_Modifier
    Dim maconnexion As New Connexion
    Private Sub Mission_Modifier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_mission_pays_mod()
    End Sub
    Private Sub bmodif_Click(sender As Object, e As EventArgs) Handles bmodif.Click
        maconnexion.modifier_mission(
            id:=tbid.Text,
            numpays:=cbnompays.SelectedItem,
            nom:=tbnommission.Text,
            descript:=tbdescrip.Text,
            datedeb:=datedeb.Value.ToShortDateString(),
            datefin:=datefin.Value.ToShortDateString())
        Me.Hide()
        Mission.Show()
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Mission.Show()
    End Sub
End Class