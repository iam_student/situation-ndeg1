﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mission
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mission))
        Me.dgvmissions = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NumPays = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NomM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateCreaM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDebutM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateFinM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lmissions = New System.Windows.Forms.Label()
        Me.bmodifier = New System.Windows.Forms.Button()
        Me.bsupprimer = New System.Windows.Forms.Button()
        Me.bnouvelle = New System.Windows.Forms.Button()
        Me.lmembre = New System.Windows.Forms.Label()
        Me.bmenu = New System.Windows.Forms.Button()
        Me.Baffectermembre = New System.Windows.Forms.Button()
        Me.btsupmembre = New System.Windows.Forms.Button()
        Me.lvmembre = New System.Windows.Forms.ListView()
        Me.ID_Mission = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ID_Membre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Nom_Membre = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        CType(Me.dgvmissions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvmissions
        '
        Me.dgvmissions.AllowUserToAddRows = False
        Me.dgvmissions.AllowUserToDeleteRows = False
        Me.dgvmissions.AllowUserToResizeColumns = False
        Me.dgvmissions.AllowUserToResizeRows = False
        Me.dgvmissions.BackgroundColor = System.Drawing.Color.White
        Me.dgvmissions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvmissions.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.NumPays, Me.NomM, Me.DescM, Me.DateCreaM, Me.DateDebutM, Me.DateFinM})
        Me.dgvmissions.Cursor = System.Windows.Forms.Cursors.Default
        Me.dgvmissions.Location = New System.Drawing.Point(12, 65)
        Me.dgvmissions.Name = "dgvmissions"
        Me.dgvmissions.Size = New System.Drawing.Size(744, 184)
        Me.dgvmissions.TabIndex = 0
        '
        'ID
        '
        Me.ID.Frozen = True
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'NumPays
        '
        Me.NumPays.Frozen = True
        Me.NumPays.HeaderText = "NumPays"
        Me.NumPays.Name = "NumPays"
        Me.NumPays.ReadOnly = True
        '
        'NomM
        '
        Me.NomM.Frozen = True
        Me.NomM.HeaderText = "NomM"
        Me.NomM.Name = "NomM"
        Me.NomM.ReadOnly = True
        '
        'DescM
        '
        Me.DescM.Frozen = True
        Me.DescM.HeaderText = "DescM"
        Me.DescM.Name = "DescM"
        Me.DescM.ReadOnly = True
        '
        'DateCreaM
        '
        Me.DateCreaM.Frozen = True
        Me.DateCreaM.HeaderText = "DateCreaM"
        Me.DateCreaM.Name = "DateCreaM"
        Me.DateCreaM.ReadOnly = True
        '
        'DateDebutM
        '
        Me.DateDebutM.Frozen = True
        Me.DateDebutM.HeaderText = "DateDebutM"
        Me.DateDebutM.Name = "DateDebutM"
        Me.DateDebutM.ReadOnly = True
        '
        'DateFinM
        '
        Me.DateFinM.Frozen = True
        Me.DateFinM.HeaderText = "DateFinM"
        Me.DateFinM.Name = "DateFinM"
        Me.DateFinM.ReadOnly = True
        '
        'lmissions
        '
        Me.lmissions.AutoSize = True
        Me.lmissions.BackColor = System.Drawing.Color.White
        Me.lmissions.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmissions.ForeColor = System.Drawing.Color.Black
        Me.lmissions.Location = New System.Drawing.Point(12, 9)
        Me.lmissions.Name = "lmissions"
        Me.lmissions.Size = New System.Drawing.Size(147, 31)
        Me.lmissions.TabIndex = 1
        Me.lmissions.Text = "MISSIONS"
        '
        'bmodifier
        '
        Me.bmodifier.BackColor = System.Drawing.Color.White
        Me.bmodifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodifier.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmodifier.Location = New System.Drawing.Point(238, 258)
        Me.bmodifier.Name = "bmodifier"
        Me.bmodifier.Size = New System.Drawing.Size(132, 58)
        Me.bmodifier.TabIndex = 2
        Me.bmodifier.Text = "MODIFIER"
        Me.bmodifier.UseVisualStyleBackColor = False
        '
        'bsupprimer
        '
        Me.bsupprimer.BackColor = System.Drawing.Color.White
        Me.bsupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsupprimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsupprimer.Location = New System.Drawing.Point(376, 258)
        Me.bsupprimer.Name = "bsupprimer"
        Me.bsupprimer.Size = New System.Drawing.Size(141, 58)
        Me.bsupprimer.TabIndex = 3
        Me.bsupprimer.Text = "SUPPRIMER"
        Me.bsupprimer.UseVisualStyleBackColor = False
        '
        'bnouvelle
        '
        Me.bnouvelle.BackColor = System.Drawing.Color.White
        Me.bnouvelle.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouvelle.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouvelle.Location = New System.Drawing.Point(12, 258)
        Me.bnouvelle.Name = "bnouvelle"
        Me.bnouvelle.Size = New System.Drawing.Size(220, 58)
        Me.bnouvelle.TabIndex = 4
        Me.bnouvelle.Text = "NOUVELLE MISSION"
        Me.bnouvelle.UseVisualStyleBackColor = False
        '
        'lmembre
        '
        Me.lmembre.AutoSize = True
        Me.lmembre.BackColor = System.Drawing.Color.White
        Me.lmembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmembre.ForeColor = System.Drawing.Color.Black
        Me.lmembre.Location = New System.Drawing.Point(759, 45)
        Me.lmembre.Name = "lmembre"
        Me.lmembre.Size = New System.Drawing.Size(66, 17)
        Me.lmembre.TabIndex = 6
        Me.lmembre.Text = "Membres"
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(948, 264)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(151, 47)
        Me.bmenu.TabIndex = 7
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'Baffectermembre
        '
        Me.Baffectermembre.BackColor = System.Drawing.Color.White
        Me.Baffectermembre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Baffectermembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Baffectermembre.Location = New System.Drawing.Point(523, 258)
        Me.Baffectermembre.Name = "Baffectermembre"
        Me.Baffectermembre.Size = New System.Drawing.Size(419, 58)
        Me.Baffectermembre.TabIndex = 8
        Me.Baffectermembre.Text = "AFFECTER UNE MISSION A UN MEMBRE"
        Me.Baffectermembre.UseVisualStyleBackColor = False
        '
        'btsupmembre
        '
        Me.btsupmembre.BackColor = System.Drawing.Color.White
        Me.btsupmembre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btsupmembre.Location = New System.Drawing.Point(1017, 96)
        Me.btsupmembre.Name = "btsupmembre"
        Me.btsupmembre.Size = New System.Drawing.Size(82, 68)
        Me.btsupmembre.TabIndex = 10
        Me.btsupmembre.Text = "SUPPRIMER UN MEMBRE"
        Me.btsupmembre.UseVisualStyleBackColor = False
        '
        'lvmembre
        '
        Me.lvmembre.BackColor = System.Drawing.Color.White
        Me.lvmembre.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ID_Mission, Me.ID_Membre, Me.Nom_Membre})
        Me.lvmembre.FullRowSelect = True
        Me.lvmembre.Location = New System.Drawing.Point(762, 65)
        Me.lvmembre.Name = "lvmembre"
        Me.lvmembre.Size = New System.Drawing.Size(249, 184)
        Me.lvmembre.TabIndex = 11
        Me.lvmembre.UseCompatibleStateImageBehavior = False
        Me.lvmembre.View = System.Windows.Forms.View.Details
        '
        'ID_Mission
        '
        Me.ID_Mission.Text = "ID Mission"
        Me.ID_Mission.Width = 63
        '
        'ID_Membre
        '
        Me.ID_Membre.Text = "ID Membre"
        Me.ID_Membre.Width = 97
        '
        'Nom_Membre
        '
        Me.Nom_Membre.Text = "Nom Membre"
        Me.Nom_Membre.Width = 107
        '
        'Mission
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1111, 328)
        Me.Controls.Add(Me.lvmembre)
        Me.Controls.Add(Me.btsupmembre)
        Me.Controls.Add(Me.Baffectermembre)
        Me.Controls.Add(Me.bmenu)
        Me.Controls.Add(Me.lmembre)
        Me.Controls.Add(Me.bnouvelle)
        Me.Controls.Add(Me.bsupprimer)
        Me.Controls.Add(Me.bmodifier)
        Me.Controls.Add(Me.lmissions)
        Me.Controls.Add(Me.dgvmissions)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Mission"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mission"
        CType(Me.dgvmissions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvmissions As System.Windows.Forms.DataGridView
    Friend WithEvents lmissions As System.Windows.Forms.Label
    Friend WithEvents bmodifier As System.Windows.Forms.Button
    Friend WithEvents bsupprimer As System.Windows.Forms.Button
    Friend WithEvents bnouvelle As System.Windows.Forms.Button
    Friend WithEvents lmembre As System.Windows.Forms.Label
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents Baffectermembre As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NumPays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NomM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateCreaM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateDebutM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateFinM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btsupmembre As System.Windows.Forms.Button
    Friend WithEvents lvmembre As System.Windows.Forms.ListView
    Friend WithEvents ID_Mission As System.Windows.Forms.ColumnHeader
    Friend WithEvents ID_Membre As System.Windows.Forms.ColumnHeader
    Friend WithEvents Nom_Membre As System.Windows.Forms.ColumnHeader
End Class
