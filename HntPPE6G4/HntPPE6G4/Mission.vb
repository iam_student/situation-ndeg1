﻿Public Class Mission
    Dim maConnexion As New Connexion()
    Private Sub Mission_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maConnexion.remplir_mission()
        bmodifier.Visible = False
        bsupprimer.Visible = False
        Baffectermembre.Visible = False
        btsupmembre.Visible = False
    End Sub
   
    Private Sub bmodifier_Click(sender As Object, e As EventArgs) Handles bmodifier.Click
        Dim NumLigne As Integer
        Dim IdLigne As String
        Dim NomM As String
        Dim descM As String
        With dgvmissions
            NumLigne = .CurrentCell.RowIndex
            IdLigne = .Rows(NumLigne).Cells(0).Value
            maConnexion.remplir_mission_pays()
            NomM = .Rows(NumLigne).Cells(2).Value
            descM = .Rows(NumLigne).Cells(3).Value
            Mission_Modifier.tbid.Text = IdLigne
            Mission_Modifier.tbnommission.Text = NomM
            Mission_Modifier.tbdescrip.Text = descM

        End With
        Me.Close()
        Mission_Modifier.Show()
    End Sub

    Private Sub bnouvelle_Click(sender As Object, e As EventArgs) Handles bnouvelle.Click
        Me.Close()
        Mission_Ajouter.Show()
    End Sub

    Private Sub bmenu_Click(sender As Object, e As EventArgs) Handles bmenu.Click
        Me.Close()
        Menu_app.Show()
    End Sub

    Private Sub bsupprimer_Click(sender As Object, e As EventArgs) Handles bsupprimer.Click
        Dim NumLigne As Integer
        Dim ValLigne1 As String
        With dgvmissions
            NumLigne = .CurrentCell.RowIndex
            ValLigne1 = .Rows(NumLigne).Cells(0).Value
            If MsgBox("Etes vous sur de vouloir supprimer la mission : " + ValLigne1 + " ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maConnexion.supprimer_mission(ValLigne1)
            End If
        End With
        Me.Close()
        Mission_Chargement.Show()
    End Sub

    Private Sub Baffectermembre_Click(sender As Object, e As EventArgs) Handles Baffectermembre.Click
        Dim NumLigne As Integer
        Dim IdLigne As String
        With dgvmissions
            NumLigne = .CurrentCell.RowIndex
            IdLigne = .Rows(NumLigne).Cells(0).Value
            Mission_Ajouter_membre.tbid.Text = IdLigne
        End With
        Me.Close()
        Mission_Ajouter_membre.Show()
    End Sub

    Private Sub dgvmissions_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvmissions.CellMouseClick
        bmodifier.Visible = True
        bsupprimer.Visible = True
        Baffectermembre.Visible = True
        Me.lvmembre.Items.Clear()
        Dim NumLigne As Integer
        Dim ValLigne1 As String
        With dgvmissions
            NumLigne = .CurrentCell.RowIndex
            ValLigne1 = .Rows(NumLigne).Cells(0).Value
        End With
        maConnexion.remplir_liste_membre(idmission:=ValLigne1)
    End Sub

    Private Sub btsupmembre_Click(sender As Object, e As EventArgs) Handles btsupmembre.Click
        Dim NumLigne As Integer
        Dim ValId As String
        Dim ValIDMembre As String
        With lvmembre
            NumLigne = .SelectedIndices(0)
            ValId = .Items(NumLigne).SubItems(0).Text
            ValIDMembre = .Items(NumLigne).SubItems(1).Text
            If MsgBox("Etes vous sur de vouloir supprimer le membre : " + ValIDMembre + " de la mission ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maConnexion.supprimer_membre_mission(NumM:=ValId, NumP:=ValIDMembre)
            End If
        End With
        Me.Close()
        Mission_Chargement.Show()
    End Sub

    Private Sub lvmembre_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvmembre.SelectedIndexChanged
        btsupmembre.Visible = True
    End Sub
End Class