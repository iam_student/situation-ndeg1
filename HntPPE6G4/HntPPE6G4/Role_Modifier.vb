﻿Public Class Role_Modifier
    Dim maconnexion As New Connexion
    Private Sub Role_Modifier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_role_spec()
    End Sub
    Private Sub bmodif_Click(sender As Object, e As EventArgs) Handles bmodif.Click
        maconnexion.modifier_role(tbcoder.Text, tblibrole.Text,cbcodespec.SelectedItem)
        Me.Hide()
        Role.Show()
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Role.Show()
    End Sub
End Class