﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mission_Ajouter_membre
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mission_Ajouter_membre))
        Me.lmissionajoutmembre = New System.Windows.Forms.Label()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.cbmembre = New System.Windows.Forms.ComboBox()
        Me.lnommembre = New System.Windows.Forms.Label()
        Me.cbrole = New System.Windows.Forms.ComboBox()
        Me.lnomrole = New System.Windows.Forms.Label()
        Me.bajout = New System.Windows.Forms.Button()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lmissionajoutmembre
        '
        Me.lmissionajoutmembre.AutoSize = True
        Me.lmissionajoutmembre.BackColor = System.Drawing.Color.White
        Me.lmissionajoutmembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmissionajoutmembre.Location = New System.Drawing.Point(12, 9)
        Me.lmissionajoutmembre.Name = "lmissionajoutmembre"
        Me.lmissionajoutmembre.Size = New System.Drawing.Size(415, 31)
        Me.lmissionajoutmembre.TabIndex = 10
        Me.lmissionajoutmembre.Text = "Ajouter un Membre à une Mission"
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(297, 57)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 23
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(195, 58)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 22
        Me.lid.Text = "ID : "
        '
        'cbmembre
        '
        Me.cbmembre.FormattingEnabled = True
        Me.cbmembre.Location = New System.Drawing.Point(297, 85)
        Me.cbmembre.Name = "cbmembre"
        Me.cbmembre.Size = New System.Drawing.Size(121, 21)
        Me.cbmembre.TabIndex = 30
        '
        'lnommembre
        '
        Me.lnommembre.AutoSize = True
        Me.lnommembre.BackColor = System.Drawing.Color.White
        Me.lnommembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnommembre.Location = New System.Drawing.Point(195, 86)
        Me.lnommembre.Name = "lnommembre"
        Me.lnommembre.Size = New System.Drawing.Size(96, 17)
        Me.lnommembre.TabIndex = 29
        Me.lnommembre.Text = "Nom membre:"
        '
        'cbrole
        '
        Me.cbrole.FormattingEnabled = True
        Me.cbrole.Location = New System.Drawing.Point(297, 122)
        Me.cbrole.Name = "cbrole"
        Me.cbrole.Size = New System.Drawing.Size(121, 21)
        Me.cbrole.TabIndex = 32
        '
        'lnomrole
        '
        Me.lnomrole.AutoSize = True
        Me.lnomrole.BackColor = System.Drawing.Color.White
        Me.lnomrole.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnomrole.Location = New System.Drawing.Point(195, 123)
        Me.lnomrole.Name = "lnomrole"
        Me.lnomrole.Size = New System.Drawing.Size(74, 17)
        Me.lnomrole.TabIndex = 31
        Me.lnomrole.Text = "Nom Role:"
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(198, 164)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(88, 35)
        Me.bajout.TabIndex = 34
        Me.bajout.Text = "AJOUTER"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(343, 176)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Mission_Ajouter_membre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(430, 211)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.cbrole)
        Me.Controls.Add(Me.lnomrole)
        Me.Controls.Add(Me.cbmembre)
        Me.Controls.Add(Me.lnommembre)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.lmissionajoutmembre)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Mission_Ajouter_membre"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mission_Ajouter_Membre"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lmissionajoutmembre As System.Windows.Forms.Label
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents lid As System.Windows.Forms.Label
    Friend WithEvents cbmembre As System.Windows.Forms.ComboBox
    Friend WithEvents lnommembre As System.Windows.Forms.Label
    Friend WithEvents cbrole As System.Windows.Forms.ComboBox
    Friend WithEvents lnomrole As System.Windows.Forms.Label
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents bretour As Button
End Class
