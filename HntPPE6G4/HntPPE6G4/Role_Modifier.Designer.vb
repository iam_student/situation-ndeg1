﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Role_Modifier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Role_Modifier))
        Me.bmodif = New System.Windows.Forms.Button()
        Me.tbcoder = New System.Windows.Forms.TextBox()
        Me.lcoder = New System.Windows.Forms.Label()
        Me.lrolemodif = New System.Windows.Forms.Label()
        Me.tblibrole = New System.Windows.Forms.TextBox()
        Me.llibrole = New System.Windows.Forms.Label()
        Me.lcodespec = New System.Windows.Forms.Label()
        Me.cbcodespec = New System.Windows.Forms.ComboBox()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bmodif
        '
        Me.bmodif.BackColor = System.Drawing.Color.White
        Me.bmodif.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodif.Location = New System.Drawing.Point(150, 143)
        Me.bmodif.Name = "bmodif"
        Me.bmodif.Size = New System.Drawing.Size(118, 30)
        Me.bmodif.TabIndex = 23
        Me.bmodif.Text = "Modifier"
        Me.bmodif.UseVisualStyleBackColor = False
        '
        'tbcoder
        '
        Me.tbcoder.Enabled = False
        Me.tbcoder.Location = New System.Drawing.Point(153, 54)
        Me.tbcoder.Name = "tbcoder"
        Me.tbcoder.Size = New System.Drawing.Size(100, 20)
        Me.tbcoder.TabIndex = 22
        '
        'lcoder
        '
        Me.lcoder.AutoSize = True
        Me.lcoder.BackColor = System.Drawing.Color.White
        Me.lcoder.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lcoder.Location = New System.Drawing.Point(15, 55)
        Me.lcoder.Name = "lcoder"
        Me.lcoder.Size = New System.Drawing.Size(77, 17)
        Me.lcoder.TabIndex = 21
        Me.lcoder.Text = "Code rôle :"
        '
        'lrolemodif
        '
        Me.lrolemodif.AutoSize = True
        Me.lrolemodif.BackColor = System.Drawing.Color.White
        Me.lrolemodif.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lrolemodif.Location = New System.Drawing.Point(12, 9)
        Me.lrolemodif.Name = "lrolemodif"
        Me.lrolemodif.Size = New System.Drawing.Size(210, 31)
        Me.lrolemodif.TabIndex = 20
        Me.lrolemodif.Text = "Modifier un Rôle"
        '
        'tblibrole
        '
        Me.tblibrole.Location = New System.Drawing.Point(153, 81)
        Me.tblibrole.Name = "tblibrole"
        Me.tblibrole.Size = New System.Drawing.Size(100, 20)
        Me.tblibrole.TabIndex = 25
        '
        'llibrole
        '
        Me.llibrole.AutoSize = True
        Me.llibrole.BackColor = System.Drawing.Color.White
        Me.llibrole.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llibrole.Location = New System.Drawing.Point(15, 82)
        Me.llibrole.Name = "llibrole"
        Me.llibrole.Size = New System.Drawing.Size(57, 17)
        Me.llibrole.TabIndex = 24
        Me.llibrole.Text = "Libellé :"
        '
        'lcodespec
        '
        Me.lcodespec.AutoSize = True
        Me.lcodespec.BackColor = System.Drawing.Color.White
        Me.lcodespec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lcodespec.Location = New System.Drawing.Point(15, 108)
        Me.lcodespec.Name = "lcodespec"
        Me.lcodespec.Size = New System.Drawing.Size(114, 17)
        Me.lcodespec.TabIndex = 26
        Me.lcodespec.Text = "Code Spécialité :"
        '
        'cbcodespec
        '
        Me.cbcodespec.FormattingEnabled = True
        Me.cbcodespec.Location = New System.Drawing.Point(150, 107)
        Me.cbcodespec.Name = "cbcodespec"
        Me.cbcodespec.Size = New System.Drawing.Size(121, 21)
        Me.cbcodespec.TabIndex = 27
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(12, 169)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Role_Modifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(283, 204)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.cbcodespec)
        Me.Controls.Add(Me.lcodespec)
        Me.Controls.Add(Me.tblibrole)
        Me.Controls.Add(Me.llibrole)
        Me.Controls.Add(Me.bmodif)
        Me.Controls.Add(Me.tbcoder)
        Me.Controls.Add(Me.lcoder)
        Me.Controls.Add(Me.lrolemodif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Role_Modifier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Role_Modifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bmodif As System.Windows.Forms.Button
    Friend WithEvents tbcoder As System.Windows.Forms.TextBox
    Friend WithEvents lcoder As System.Windows.Forms.Label
    Friend WithEvents lrolemodif As System.Windows.Forms.Label
    Friend WithEvents tblibrole As System.Windows.Forms.TextBox
    Friend WithEvents llibrole As System.Windows.Forms.Label
    Friend WithEvents lcodespec As System.Windows.Forms.Label
    Friend WithEvents cbcodespec As System.Windows.Forms.ComboBox
    Friend WithEvents bretour As Button
End Class
