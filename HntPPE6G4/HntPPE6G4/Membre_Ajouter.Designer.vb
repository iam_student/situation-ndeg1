﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Membre_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Membre_Ajouter))
        Me.cbdon = New System.Windows.Forms.CheckBox()
        Me.cbbenevole = New System.Windows.Forms.CheckBox()
        Me.tbgsm = New System.Windows.Forms.TextBox()
        Me.lgsm = New System.Windows.Forms.Label()
        Me.bajout = New System.Windows.Forms.Button()
        Me.datesortie = New System.Windows.Forms.DateTimePicker()
        Me.dateentre = New System.Windows.Forms.DateTimePicker()
        Me.tbadresse = New System.Windows.Forms.TextBox()
        Me.tbprenommembre = New System.Windows.Forms.TextBox()
        Me.tbnommembre = New System.Windows.Forms.TextBox()
        Me.ldatesortie = New System.Windows.Forms.Label()
        Me.ldateentre = New System.Windows.Forms.Label()
        Me.ladresse = New System.Windows.Forms.Label()
        Me.lprenommembre = New System.Windows.Forms.Label()
        Me.lnommembre = New System.Windows.Forms.Label()
        Me.lmembre_ajout = New System.Windows.Forms.Label()
        Me.cbspe = New System.Windows.Forms.ComboBox()
        Me.lspe = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbdon
        '
        Me.cbdon.AutoSize = True
        Me.cbdon.BackColor = System.Drawing.Color.White
        Me.cbdon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbdon.Location = New System.Drawing.Point(339, 191)
        Me.cbdon.Name = "cbdon"
        Me.cbdon.Size = New System.Drawing.Size(86, 21)
        Me.cbdon.TabIndex = 43
        Me.cbdon.Text = "Donateur"
        Me.cbdon.UseVisualStyleBackColor = False
        '
        'cbbenevole
        '
        Me.cbbenevole.AutoSize = True
        Me.cbbenevole.BackColor = System.Drawing.Color.White
        Me.cbbenevole.Checked = True
        Me.cbbenevole.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbbenevole.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbbenevole.Location = New System.Drawing.Point(340, 90)
        Me.cbbenevole.Name = "cbbenevole"
        Me.cbbenevole.Size = New System.Drawing.Size(86, 21)
        Me.cbbenevole.TabIndex = 42
        Me.cbbenevole.Text = "Bénévole"
        Me.cbbenevole.UseVisualStyleBackColor = False
        '
        'tbgsm
        '
        Me.tbgsm.Location = New System.Drawing.Point(124, 187)
        Me.tbgsm.MaxLength = 10
        Me.tbgsm.Name = "tbgsm"
        Me.tbgsm.Size = New System.Drawing.Size(100, 20)
        Me.tbgsm.TabIndex = 41
        '
        'lgsm
        '
        Me.lgsm.AutoSize = True
        Me.lgsm.BackColor = System.Drawing.Color.White
        Me.lgsm.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lgsm.Location = New System.Drawing.Point(10, 188)
        Me.lgsm.Name = "lgsm"
        Me.lgsm.Size = New System.Drawing.Size(84, 17)
        Me.lgsm.TabIndex = 40
        Me.lgsm.Text = "Téléphone :"
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(214, 246)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(88, 35)
        Me.bajout.TabIndex = 39
        Me.bajout.Text = "AJOUTER"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'datesortie
        '
        Me.datesortie.Location = New System.Drawing.Point(465, 136)
        Me.datesortie.Name = "datesortie"
        Me.datesortie.Size = New System.Drawing.Size(200, 20)
        Me.datesortie.TabIndex = 38
        '
        'dateentre
        '
        Me.dateentre.Location = New System.Drawing.Point(465, 110)
        Me.dateentre.Name = "dateentre"
        Me.dateentre.Size = New System.Drawing.Size(200, 20)
        Me.dateentre.TabIndex = 37
        '
        'tbadresse
        '
        Me.tbadresse.Location = New System.Drawing.Point(124, 158)
        Me.tbadresse.Name = "tbadresse"
        Me.tbadresse.Size = New System.Drawing.Size(100, 20)
        Me.tbadresse.TabIndex = 36
        '
        'tbprenommembre
        '
        Me.tbprenommembre.Location = New System.Drawing.Point(124, 124)
        Me.tbprenommembre.Name = "tbprenommembre"
        Me.tbprenommembre.Size = New System.Drawing.Size(100, 20)
        Me.tbprenommembre.TabIndex = 35
        '
        'tbnommembre
        '
        Me.tbnommembre.Location = New System.Drawing.Point(124, 90)
        Me.tbnommembre.Name = "tbnommembre"
        Me.tbnommembre.Size = New System.Drawing.Size(100, 20)
        Me.tbnommembre.TabIndex = 34
        '
        'ldatesortie
        '
        Me.ldatesortie.AutoSize = True
        Me.ldatesortie.BackColor = System.Drawing.Color.White
        Me.ldatesortie.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatesortie.Location = New System.Drawing.Point(357, 140)
        Me.ldatesortie.Name = "ldatesortie"
        Me.ldatesortie.Size = New System.Drawing.Size(105, 17)
        Me.ldatesortie.TabIndex = 33
        Me.ldatesortie.Text = "Date de sortie :"
        '
        'ldateentre
        '
        Me.ldateentre.AutoSize = True
        Me.ldateentre.BackColor = System.Drawing.Color.White
        Me.ldateentre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldateentre.Location = New System.Drawing.Point(357, 114)
        Me.ldateentre.Name = "ldateentre"
        Me.ldateentre.Size = New System.Drawing.Size(90, 17)
        Me.ldateentre.TabIndex = 32
        Me.ldateentre.Text = "Date d'entré:"
        '
        'ladresse
        '
        Me.ladresse.AutoSize = True
        Me.ladresse.BackColor = System.Drawing.Color.White
        Me.ladresse.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ladresse.Location = New System.Drawing.Point(10, 159)
        Me.ladresse.Name = "ladresse"
        Me.ladresse.Size = New System.Drawing.Size(68, 17)
        Me.ladresse.TabIndex = 31
        Me.ladresse.Text = "Adresse :"
        '
        'lprenommembre
        '
        Me.lprenommembre.AutoSize = True
        Me.lprenommembre.BackColor = System.Drawing.Color.White
        Me.lprenommembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lprenommembre.Location = New System.Drawing.Point(10, 124)
        Me.lprenommembre.Name = "lprenommembre"
        Me.lprenommembre.Size = New System.Drawing.Size(65, 17)
        Me.lprenommembre.TabIndex = 30
        Me.lprenommembre.Text = "Prénom :"
        '
        'lnommembre
        '
        Me.lnommembre.AutoSize = True
        Me.lnommembre.BackColor = System.Drawing.Color.White
        Me.lnommembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnommembre.Location = New System.Drawing.Point(10, 91)
        Me.lnommembre.Name = "lnommembre"
        Me.lnommembre.Size = New System.Drawing.Size(45, 17)
        Me.lnommembre.TabIndex = 29
        Me.lnommembre.Text = "Nom :"
        '
        'lmembre_ajout
        '
        Me.lmembre_ajout.AutoSize = True
        Me.lmembre_ajout.BackColor = System.Drawing.Color.White
        Me.lmembre_ajout.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmembre_ajout.Location = New System.Drawing.Point(12, 9)
        Me.lmembre_ajout.Name = "lmembre_ajout"
        Me.lmembre_ajout.Size = New System.Drawing.Size(242, 31)
        Me.lmembre_ajout.TabIndex = 28
        Me.lmembre_ajout.Text = "Ajouter un membre"
        '
        'cbspe
        '
        Me.cbspe.FormattingEnabled = True
        Me.cbspe.Location = New System.Drawing.Point(465, 162)
        Me.cbspe.Name = "cbspe"
        Me.cbspe.Size = New System.Drawing.Size(121, 21)
        Me.cbspe.TabIndex = 45
        '
        'lspe
        '
        Me.lspe.AutoSize = True
        Me.lspe.BackColor = System.Drawing.Color.White
        Me.lspe.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lspe.Location = New System.Drawing.Point(357, 163)
        Me.lspe.Name = "lspe"
        Me.lspe.Size = New System.Drawing.Size(77, 17)
        Me.lspe.TabIndex = 44
        Me.lspe.Text = "Spécialité :"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(594, 287)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 46
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Membre_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(681, 322)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.cbspe)
        Me.Controls.Add(Me.lspe)
        Me.Controls.Add(Me.cbdon)
        Me.Controls.Add(Me.cbbenevole)
        Me.Controls.Add(Me.tbgsm)
        Me.Controls.Add(Me.lgsm)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.datesortie)
        Me.Controls.Add(Me.dateentre)
        Me.Controls.Add(Me.tbadresse)
        Me.Controls.Add(Me.tbprenommembre)
        Me.Controls.Add(Me.tbnommembre)
        Me.Controls.Add(Me.ldatesortie)
        Me.Controls.Add(Me.ldateentre)
        Me.Controls.Add(Me.ladresse)
        Me.Controls.Add(Me.lprenommembre)
        Me.Controls.Add(Me.lnommembre)
        Me.Controls.Add(Me.lmembre_ajout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Membre_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Membre_Ajouter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cbdon As System.Windows.Forms.CheckBox
    Friend WithEvents cbbenevole As System.Windows.Forms.CheckBox
    Friend WithEvents tbgsm As System.Windows.Forms.TextBox
    Friend WithEvents lgsm As System.Windows.Forms.Label
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents datesortie As System.Windows.Forms.DateTimePicker
    Friend WithEvents dateentre As System.Windows.Forms.DateTimePicker
    Friend WithEvents tbadresse As System.Windows.Forms.TextBox
    Friend WithEvents tbprenommembre As System.Windows.Forms.TextBox
    Friend WithEvents tbnommembre As System.Windows.Forms.TextBox
    Friend WithEvents ldatesortie As System.Windows.Forms.Label
    Friend WithEvents ldateentre As System.Windows.Forms.Label
    Friend WithEvents ladresse As System.Windows.Forms.Label
    Friend WithEvents lprenommembre As System.Windows.Forms.Label
    Friend WithEvents lnommembre As System.Windows.Forms.Label
    Friend WithEvents lmembre_ajout As System.Windows.Forms.Label
    Friend WithEvents cbspe As System.Windows.Forms.ComboBox
    Friend WithEvents lspe As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class
