﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Vaccin
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Vaccin))
        Me.bnouveau = New System.Windows.Forms.Button()
        Me.bsupprimer = New System.Windows.Forms.Button()
        Me.bmodifier = New System.Windows.Forms.Button()
        Me.lvaccin = New System.Windows.Forms.Label()
        Me.dgvvaccin = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bmenu = New System.Windows.Forms.Button()
        CType(Me.dgvvaccin, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnouveau
        '
        Me.bnouveau.BackColor = System.Drawing.Color.White
        Me.bnouveau.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouveau.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouveau.Location = New System.Drawing.Point(260, 65)
        Me.bnouveau.Name = "bnouveau"
        Me.bnouveau.Size = New System.Drawing.Size(220, 58)
        Me.bnouveau.TabIndex = 14
        Me.bnouveau.Text = "NOUVEAU VACCIN"
        Me.bnouveau.UseVisualStyleBackColor = False
        '
        'bsupprimer
        '
        Me.bsupprimer.BackColor = System.Drawing.Color.White
        Me.bsupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsupprimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsupprimer.Location = New System.Drawing.Point(261, 191)
        Me.bsupprimer.Name = "bsupprimer"
        Me.bsupprimer.Size = New System.Drawing.Size(219, 58)
        Me.bsupprimer.TabIndex = 13
        Me.bsupprimer.Text = "SUPPRIMER"
        Me.bsupprimer.UseVisualStyleBackColor = False
        '
        'bmodifier
        '
        Me.bmodifier.BackColor = System.Drawing.Color.White
        Me.bmodifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodifier.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmodifier.Location = New System.Drawing.Point(261, 129)
        Me.bmodifier.Name = "bmodifier"
        Me.bmodifier.Size = New System.Drawing.Size(219, 56)
        Me.bmodifier.TabIndex = 12
        Me.bmodifier.Text = "MODIFIER"
        Me.bmodifier.UseVisualStyleBackColor = False
        '
        'lvaccin
        '
        Me.lvaccin.AutoSize = True
        Me.lvaccin.BackColor = System.Drawing.Color.White
        Me.lvaccin.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvaccin.Location = New System.Drawing.Point(12, 9)
        Me.lvaccin.Name = "lvaccin"
        Me.lvaccin.Size = New System.Drawing.Size(118, 31)
        Me.lvaccin.TabIndex = 11
        Me.lvaccin.Text = "VACCIN"
        '
        'dgvvaccin
        '
        Me.dgvvaccin.AllowUserToAddRows = False
        Me.dgvvaccin.AllowUserToDeleteRows = False
        Me.dgvvaccin.AllowUserToResizeColumns = False
        Me.dgvvaccin.AllowUserToResizeRows = False
        Me.dgvvaccin.BackgroundColor = System.Drawing.Color.White
        Me.dgvvaccin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvvaccin.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Description})
        Me.dgvvaccin.Location = New System.Drawing.Point(12, 65)
        Me.dgvvaccin.Name = "dgvvaccin"
        Me.dgvvaccin.Size = New System.Drawing.Size(242, 184)
        Me.dgvvaccin.TabIndex = 10
        '
        'ID
        '
        Me.ID.Frozen = True
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'Description
        '
        Me.Description.Frozen = True
        Me.Description.HeaderText = "Description"
        Me.Description.Name = "Description"
        Me.Description.ReadOnly = True
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(369, 255)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(151, 47)
        Me.bmenu.TabIndex = 15
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'Vaccin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(532, 317)
        Me.Controls.Add(Me.bmenu)
        Me.Controls.Add(Me.bnouveau)
        Me.Controls.Add(Me.bsupprimer)
        Me.Controls.Add(Me.bmodifier)
        Me.Controls.Add(Me.lvaccin)
        Me.Controls.Add(Me.dgvvaccin)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Vaccin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vaccin"
        CType(Me.dgvvaccin, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnouveau As System.Windows.Forms.Button
    Friend WithEvents bsupprimer As System.Windows.Forms.Button
    Friend WithEvents bmodifier As System.Windows.Forms.Button
    Friend WithEvents lvaccin As System.Windows.Forms.Label
    Friend WithEvents dgvvaccin As System.Windows.Forms.DataGridView
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Description As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
