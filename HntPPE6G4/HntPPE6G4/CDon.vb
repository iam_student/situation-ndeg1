﻿Public Class CDon

    Private m_dateDon As Date
    Private m_montant As Integer
    Private m_numPers As Integer

    Public Sub New(NumPers, dateD, Montant)
        m_montant = Montant
        m_dateDon = dateD
        m_numPers = NumPers
    End Sub

    Property getdateD
        Get
            Return m_dateDon
        End Get
        Set(ByVal Value)
            m_dateDon = Value
        End Set
    End Property

    Property getMontant
        Get
            Return m_montant
        End Get
        Set(ByVal Value)
            m_montant = Value
        End Set
    End Property

    Property getNumP
        Get
            Return m_numPers
        End Get
        Set(ByVal Value)
            m_numPers = Value
        End Set
    End Property
  
End Class
