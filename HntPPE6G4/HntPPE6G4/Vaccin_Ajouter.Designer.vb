﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Vaccin_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Vaccin_Ajouter))
        Me.bajout = New System.Windows.Forms.Button()
        Me.tbdescrip = New System.Windows.Forms.TextBox()
        Me.ldescrip = New System.Windows.Forms.Label()
        Me.lvaccinajout = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(18, 141)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(118, 30)
        Me.bajout.TabIndex = 23
        Me.bajout.Text = "Ajouter"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'tbdescrip
        '
        Me.tbdescrip.Location = New System.Drawing.Point(123, 89)
        Me.tbdescrip.Name = "tbdescrip"
        Me.tbdescrip.Size = New System.Drawing.Size(100, 20)
        Me.tbdescrip.TabIndex = 22
        '
        'ldescrip
        '
        Me.ldescrip.AutoSize = True
        Me.ldescrip.BackColor = System.Drawing.Color.White
        Me.ldescrip.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldescrip.Location = New System.Drawing.Point(12, 90)
        Me.ldescrip.Name = "ldescrip"
        Me.ldescrip.Size = New System.Drawing.Size(87, 17)
        Me.ldescrip.TabIndex = 21
        Me.ldescrip.Text = "Description :"
        '
        'lvaccinajout
        '
        Me.lvaccinajout.AutoSize = True
        Me.lvaccinajout.BackColor = System.Drawing.Color.White
        Me.lvaccinajout.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvaccinajout.Location = New System.Drawing.Point(12, 9)
        Me.lvaccinajout.Name = "lvaccinajout"
        Me.lvaccinajout.Size = New System.Drawing.Size(226, 31)
        Me.lvaccinajout.TabIndex = 20
        Me.lvaccinajout.Text = "Ajouter un Vaccin"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(165, 148)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Vaccin_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(252, 183)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.tbdescrip)
        Me.Controls.Add(Me.ldescrip)
        Me.Controls.Add(Me.lvaccinajout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Vaccin_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vaccin_Ajouter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents tbdescrip As System.Windows.Forms.TextBox
    Friend WithEvents ldescrip As System.Windows.Forms.Label
    Friend WithEvents lvaccinajout As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class
