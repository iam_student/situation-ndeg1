﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mission_Modifier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mission_Modifier))
        Me.lmission_modif = New System.Windows.Forms.Label()
        Me.lnommission = New System.Windows.Forms.Label()
        Me.ldescript = New System.Windows.Forms.Label()
        Me.ldatedeb = New System.Windows.Forms.Label()
        Me.ldatefin = New System.Windows.Forms.Label()
        Me.tbnommission = New System.Windows.Forms.TextBox()
        Me.tbdescrip = New System.Windows.Forms.TextBox()
        Me.datedeb = New System.Windows.Forms.DateTimePicker()
        Me.datefin = New System.Windows.Forms.DateTimePicker()
        Me.bmodif = New System.Windows.Forms.Button()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.cbnompays = New System.Windows.Forms.ComboBox()
        Me.lnompays = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lmission_modif
        '
        Me.lmission_modif.AutoSize = True
        Me.lmission_modif.BackColor = System.Drawing.Color.White
        Me.lmission_modif.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmission_modif.Location = New System.Drawing.Point(15, 9)
        Me.lmission_modif.Name = "lmission_modif"
        Me.lmission_modif.Size = New System.Drawing.Size(261, 31)
        Me.lmission_modif.TabIndex = 0
        Me.lmission_modif.Text = "Modifier une mission"
        '
        'lnommission
        '
        Me.lnommission.AutoSize = True
        Me.lnommission.BackColor = System.Drawing.Color.White
        Me.lnommission.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnommission.Location = New System.Drawing.Point(25, 127)
        Me.lnommission.Name = "lnommission"
        Me.lnommission.Size = New System.Drawing.Size(92, 17)
        Me.lnommission.TabIndex = 2
        Me.lnommission.Text = "Nom mission:"
        '
        'ldescript
        '
        Me.ldescript.AutoSize = True
        Me.ldescript.BackColor = System.Drawing.Color.White
        Me.ldescript.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldescript.Location = New System.Drawing.Point(25, 165)
        Me.ldescript.Name = "ldescript"
        Me.ldescript.Size = New System.Drawing.Size(83, 17)
        Me.ldescript.TabIndex = 3
        Me.ldescript.Text = "Description:"
        '
        'ldatedeb
        '
        Me.ldatedeb.AutoSize = True
        Me.ldatedeb.BackColor = System.Drawing.Color.White
        Me.ldatedeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatedeb.Location = New System.Drawing.Point(25, 201)
        Me.ldatedeb.Name = "ldatedeb"
        Me.ldatedeb.Size = New System.Drawing.Size(82, 17)
        Me.ldatedeb.TabIndex = 4
        Me.ldatedeb.Text = "Date début:"
        '
        'ldatefin
        '
        Me.ldatefin.AutoSize = True
        Me.ldatefin.BackColor = System.Drawing.Color.White
        Me.ldatefin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatefin.Location = New System.Drawing.Point(25, 227)
        Me.ldatefin.Name = "ldatefin"
        Me.ldatefin.Size = New System.Drawing.Size(61, 17)
        Me.ldatefin.TabIndex = 5
        Me.ldatefin.Text = "Date fin:"
        '
        'tbnommission
        '
        Me.tbnommission.Location = New System.Drawing.Point(123, 126)
        Me.tbnommission.Name = "tbnommission"
        Me.tbnommission.Size = New System.Drawing.Size(100, 20)
        Me.tbnommission.TabIndex = 7
        '
        'tbdescrip
        '
        Me.tbdescrip.Location = New System.Drawing.Point(123, 164)
        Me.tbdescrip.Name = "tbdescrip"
        Me.tbdescrip.Size = New System.Drawing.Size(100, 20)
        Me.tbdescrip.TabIndex = 8
        '
        'datedeb
        '
        Me.datedeb.Location = New System.Drawing.Point(123, 197)
        Me.datedeb.Name = "datedeb"
        Me.datedeb.Size = New System.Drawing.Size(200, 20)
        Me.datedeb.TabIndex = 9
        '
        'datefin
        '
        Me.datefin.Location = New System.Drawing.Point(123, 223)
        Me.datefin.Name = "datefin"
        Me.datefin.Size = New System.Drawing.Size(200, 20)
        Me.datefin.TabIndex = 10
        '
        'bmodif
        '
        Me.bmodif.BackColor = System.Drawing.Color.White
        Me.bmodif.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodif.Location = New System.Drawing.Point(123, 272)
        Me.bmodif.Name = "bmodif"
        Me.bmodif.Size = New System.Drawing.Size(88, 35)
        Me.bmodif.TabIndex = 11
        Me.bmodif.Text = "MODIFIER"
        Me.bmodif.UseVisualStyleBackColor = False
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(123, 67)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 23
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(30, 68)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 22
        Me.lid.Text = "ID : "
        '
        'cbnompays
        '
        Me.cbnompays.FormattingEnabled = True
        Me.cbnompays.Location = New System.Drawing.Point(123, 99)
        Me.cbnompays.Name = "cbnompays"
        Me.cbnompays.Size = New System.Drawing.Size(121, 21)
        Me.cbnompays.TabIndex = 30
        '
        'lnompays
        '
        Me.lnompays.AutoSize = True
        Me.lnompays.BackColor = System.Drawing.Color.White
        Me.lnompays.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnompays.Location = New System.Drawing.Point(25, 99)
        Me.lnompays.Name = "lnompays"
        Me.lnompays.Size = New System.Drawing.Size(75, 17)
        Me.lnompays.TabIndex = 29
        Me.lnompays.Text = "Nom pays:"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(304, 284)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Mission_Modifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(391, 319)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.cbnompays)
        Me.Controls.Add(Me.lnompays)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.bmodif)
        Me.Controls.Add(Me.datefin)
        Me.Controls.Add(Me.datedeb)
        Me.Controls.Add(Me.tbdescrip)
        Me.Controls.Add(Me.tbnommission)
        Me.Controls.Add(Me.ldatefin)
        Me.Controls.Add(Me.ldatedeb)
        Me.Controls.Add(Me.ldescript)
        Me.Controls.Add(Me.lnommission)
        Me.Controls.Add(Me.lmission_modif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Mission_Modifier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mission_Modifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lmission_modif As System.Windows.Forms.Label
    Friend WithEvents lnommission As System.Windows.Forms.Label
    Friend WithEvents ldescript As System.Windows.Forms.Label
    Friend WithEvents ldatedeb As System.Windows.Forms.Label
    Friend WithEvents ldatefin As System.Windows.Forms.Label
    Friend WithEvents tbnommission As System.Windows.Forms.TextBox
    Friend WithEvents tbdescrip As System.Windows.Forms.TextBox
    Friend WithEvents datedeb As System.Windows.Forms.DateTimePicker
    Friend WithEvents datefin As System.Windows.Forms.DateTimePicker
    Friend WithEvents bmodif As System.Windows.Forms.Button
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents lid As System.Windows.Forms.Label
    Friend WithEvents cbnompays As System.Windows.Forms.ComboBox
    Friend WithEvents lnompays As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class
