﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Don_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Don_Ajouter))
        Me.ldon = New System.Windows.Forms.Label()
        Me.cbdon = New System.Windows.Forms.ComboBox()
        Me.lnomdon = New System.Windows.Forms.Label()
        Me.lmontant = New System.Windows.Forms.Label()
        Me.montant = New System.Windows.Forms.NumericUpDown()
        Me.ldatedon = New System.Windows.Forms.Label()
        Me.datedon = New System.Windows.Forms.DateTimePicker()
        Me.bvalider = New System.Windows.Forms.Button()
        CType(Me.montant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ldon
        '
        Me.ldon.AutoSize = True
        Me.ldon.BackColor = System.Drawing.Color.White
        Me.ldon.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldon.Location = New System.Drawing.Point(9, 9)
        Me.ldon.Name = "ldon"
        Me.ldon.Size = New System.Drawing.Size(194, 31)
        Me.ldon.TabIndex = 0
        Me.ldon.Text = "Ajouter un Don"
        '
        'cbdon
        '
        Me.cbdon.FormattingEnabled = True
        Me.cbdon.Location = New System.Drawing.Point(127, 76)
        Me.cbdon.Name = "cbdon"
        Me.cbdon.Size = New System.Drawing.Size(121, 21)
        Me.cbdon.TabIndex = 1
        '
        'lnomdon
        '
        Me.lnomdon.AutoSize = True
        Me.lnomdon.BackColor = System.Drawing.Color.White
        Me.lnomdon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnomdon.Location = New System.Drawing.Point(12, 77)
        Me.lnomdon.Name = "lnomdon"
        Me.lnomdon.Size = New System.Drawing.Size(75, 17)
        Me.lnomdon.TabIndex = 2
        Me.lnomdon.Text = "Donateur: "
        '
        'lmontant
        '
        Me.lmontant.AutoSize = True
        Me.lmontant.BackColor = System.Drawing.Color.White
        Me.lmontant.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmontant.Location = New System.Drawing.Point(12, 108)
        Me.lmontant.Name = "lmontant"
        Me.lmontant.Size = New System.Drawing.Size(67, 17)
        Me.lmontant.TabIndex = 3
        Me.lmontant.Text = "Montant: "
        '
        'montant
        '
        Me.montant.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.montant.Location = New System.Drawing.Point(128, 106)
        Me.montant.Maximum = New Decimal(New Integer() {-1304428544, 434162106, 542, 0})
        Me.montant.Name = "montant"
        Me.montant.Size = New System.Drawing.Size(120, 23)
        Me.montant.TabIndex = 4
        '
        'ldatedon
        '
        Me.ldatedon.AutoSize = True
        Me.ldatedon.BackColor = System.Drawing.Color.White
        Me.ldatedon.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatedon.Location = New System.Drawing.Point(12, 135)
        Me.ldatedon.Name = "ldatedon"
        Me.ldatedon.Size = New System.Drawing.Size(94, 17)
        Me.ldatedon.TabIndex = 5
        Me.ldatedon.Text = "Date du don: "
        '
        'datedon
        '
        Me.datedon.Location = New System.Drawing.Point(127, 135)
        Me.datedon.Name = "datedon"
        Me.datedon.Size = New System.Drawing.Size(200, 20)
        Me.datedon.TabIndex = 6
        '
        'bvalider
        '
        Me.bvalider.BackColor = System.Drawing.Color.White
        Me.bvalider.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bvalider.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bvalider.Location = New System.Drawing.Point(12, 178)
        Me.bvalider.Name = "bvalider"
        Me.bvalider.Size = New System.Drawing.Size(109, 35)
        Me.bvalider.TabIndex = 7
        Me.bvalider.Text = "Valider"
        Me.bvalider.UseVisualStyleBackColor = False
        '
        'Don_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(344, 225)
        Me.Controls.Add(Me.bvalider)
        Me.Controls.Add(Me.datedon)
        Me.Controls.Add(Me.ldatedon)
        Me.Controls.Add(Me.montant)
        Me.Controls.Add(Me.lmontant)
        Me.Controls.Add(Me.lnomdon)
        Me.Controls.Add(Me.cbdon)
        Me.Controls.Add(Me.ldon)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Don_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Don"
        CType(Me.montant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ldon As System.Windows.Forms.Label
    Friend WithEvents cbdon As System.Windows.Forms.ComboBox
    Friend WithEvents lnomdon As System.Windows.Forms.Label
    Friend WithEvents lmontant As System.Windows.Forms.Label
    Friend WithEvents montant As System.Windows.Forms.NumericUpDown
    Friend WithEvents ldatedon As System.Windows.Forms.Label
    Friend WithEvents datedon As System.Windows.Forms.DateTimePicker
    Friend WithEvents bvalider As System.Windows.Forms.Button
End Class
