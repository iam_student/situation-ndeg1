﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Membre_modifier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Membre_modifier))
        Me.bmodif = New System.Windows.Forms.Button()
        Me.datesortie = New System.Windows.Forms.DateTimePicker()
        Me.tbadresse = New System.Windows.Forms.TextBox()
        Me.tbprenommembre = New System.Windows.Forms.TextBox()
        Me.tbnommembre = New System.Windows.Forms.TextBox()
        Me.ldatesortie = New System.Windows.Forms.Label()
        Me.ladresse = New System.Windows.Forms.Label()
        Me.lprenommembre = New System.Windows.Forms.Label()
        Me.lnommembre = New System.Windows.Forms.Label()
        Me.lmembre_modif = New System.Windows.Forms.Label()
        Me.tbgsm = New System.Windows.Forms.TextBox()
        Me.lgsm = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'bmodif
        '
        Me.bmodif.BackColor = System.Drawing.Color.White
        Me.bmodif.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodif.Location = New System.Drawing.Point(333, 207)
        Me.bmodif.Name = "bmodif"
        Me.bmodif.Size = New System.Drawing.Size(88, 35)
        Me.bmodif.TabIndex = 23
        Me.bmodif.Text = "MODIFIER"
        Me.bmodif.UseVisualStyleBackColor = False
        '
        'datesortie
        '
        Me.datesortie.Location = New System.Drawing.Point(451, 124)
        Me.datesortie.Name = "datesortie"
        Me.datesortie.Size = New System.Drawing.Size(200, 20)
        Me.datesortie.TabIndex = 22
        '
        'tbadresse
        '
        Me.tbadresse.Location = New System.Drawing.Point(126, 158)
        Me.tbadresse.Name = "tbadresse"
        Me.tbadresse.Size = New System.Drawing.Size(100, 20)
        Me.tbadresse.TabIndex = 20
        '
        'tbprenommembre
        '
        Me.tbprenommembre.Location = New System.Drawing.Point(126, 124)
        Me.tbprenommembre.Name = "tbprenommembre"
        Me.tbprenommembre.Size = New System.Drawing.Size(100, 20)
        Me.tbprenommembre.TabIndex = 19
        '
        'tbnommembre
        '
        Me.tbnommembre.Location = New System.Drawing.Point(126, 90)
        Me.tbnommembre.Name = "tbnommembre"
        Me.tbnommembre.Size = New System.Drawing.Size(100, 20)
        Me.tbnommembre.TabIndex = 18
        '
        'ldatesortie
        '
        Me.ldatesortie.AutoSize = True
        Me.ldatesortie.BackColor = System.Drawing.Color.White
        Me.ldatesortie.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatesortie.Location = New System.Drawing.Point(330, 125)
        Me.ldatesortie.Name = "ldatesortie"
        Me.ldatesortie.Size = New System.Drawing.Size(105, 17)
        Me.ldatesortie.TabIndex = 17
        Me.ldatesortie.Text = "Date de sortie :"
        '
        'ladresse
        '
        Me.ladresse.AutoSize = True
        Me.ladresse.BackColor = System.Drawing.Color.White
        Me.ladresse.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ladresse.Location = New System.Drawing.Point(12, 159)
        Me.ladresse.Name = "ladresse"
        Me.ladresse.Size = New System.Drawing.Size(68, 17)
        Me.ladresse.TabIndex = 15
        Me.ladresse.Text = "Adresse :"
        '
        'lprenommembre
        '
        Me.lprenommembre.AutoSize = True
        Me.lprenommembre.BackColor = System.Drawing.Color.White
        Me.lprenommembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lprenommembre.Location = New System.Drawing.Point(12, 124)
        Me.lprenommembre.Name = "lprenommembre"
        Me.lprenommembre.Size = New System.Drawing.Size(65, 17)
        Me.lprenommembre.TabIndex = 14
        Me.lprenommembre.Text = "Prénom :"
        '
        'lnommembre
        '
        Me.lnommembre.AutoSize = True
        Me.lnommembre.BackColor = System.Drawing.Color.White
        Me.lnommembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnommembre.Location = New System.Drawing.Point(12, 91)
        Me.lnommembre.Name = "lnommembre"
        Me.lnommembre.Size = New System.Drawing.Size(45, 17)
        Me.lnommembre.TabIndex = 13
        Me.lnommembre.Text = "Nom :"
        '
        'lmembre_modif
        '
        Me.lmembre_modif.AutoSize = True
        Me.lmembre_modif.BackColor = System.Drawing.Color.White
        Me.lmembre_modif.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmembre_modif.Location = New System.Drawing.Point(12, 9)
        Me.lmembre_modif.Name = "lmembre_modif"
        Me.lmembre_modif.Size = New System.Drawing.Size(264, 31)
        Me.lmembre_modif.TabIndex = 12
        Me.lmembre_modif.Text = "Modifier un bénévole"
        '
        'tbgsm
        '
        Me.tbgsm.Location = New System.Drawing.Point(126, 187)
        Me.tbgsm.MaxLength = 10
        Me.tbgsm.Name = "tbgsm"
        Me.tbgsm.Size = New System.Drawing.Size(100, 20)
        Me.tbgsm.TabIndex = 25
        '
        'lgsm
        '
        Me.lgsm.AutoSize = True
        Me.lgsm.BackColor = System.Drawing.Color.White
        Me.lgsm.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lgsm.Location = New System.Drawing.Point(12, 188)
        Me.lgsm.Name = "lgsm"
        Me.lgsm.Size = New System.Drawing.Size(84, 17)
        Me.lgsm.TabIndex = 24
        Me.lgsm.Text = "Téléphone :"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(599, 219)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(126, 64)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 49
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(12, 65)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 48
        Me.lid.Text = "ID : "
        '
        'Membre_modifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(686, 254)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.tbgsm)
        Me.Controls.Add(Me.lgsm)
        Me.Controls.Add(Me.bmodif)
        Me.Controls.Add(Me.datesortie)
        Me.Controls.Add(Me.tbadresse)
        Me.Controls.Add(Me.tbprenommembre)
        Me.Controls.Add(Me.tbnommembre)
        Me.Controls.Add(Me.ldatesortie)
        Me.Controls.Add(Me.ladresse)
        Me.Controls.Add(Me.lprenommembre)
        Me.Controls.Add(Me.lnommembre)
        Me.Controls.Add(Me.lmembre_modif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Membre_modifier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Membre_modifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bmodif As System.Windows.Forms.Button
    Friend WithEvents datesortie As System.Windows.Forms.DateTimePicker
    Friend WithEvents tbadresse As System.Windows.Forms.TextBox
    Friend WithEvents tbprenommembre As System.Windows.Forms.TextBox
    Friend WithEvents tbnommembre As System.Windows.Forms.TextBox
    Friend WithEvents ldatesortie As System.Windows.Forms.Label
    Friend WithEvents ladresse As System.Windows.Forms.Label
    Friend WithEvents lprenommembre As System.Windows.Forms.Label
    Friend WithEvents lnommembre As System.Windows.Forms.Label
    Friend WithEvents lmembre_modif As System.Windows.Forms.Label
    Friend WithEvents tbgsm As System.Windows.Forms.TextBox
    Friend WithEvents lgsm As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
    Friend WithEvents tbid As TextBox
    Friend WithEvents lid As Label
End Class
