﻿Imports System.Data.SqlClient
Public Class Connexion

    Private m_Connexion As SqlConnection


    Private Sub ouvrir()
        ' Instancier la connexion
        'm_Connexion = New SqlConnection("server=AD2008;" & "database=hnt6grp4;user ID=SIO2018;password=SIO2018;")
        m_Connexion = New SqlConnection("server='172.18.158.141';" & "database=gr4ppe6;user ID=dev2;password=dev2;")
        'Ouvrir la connexion
        Try
            m_Connexion.Open()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub fermer()
        'Fermer la connexion
        Try
            m_Connexion.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    'Login
    Public Sub login(login As String, mdp As String)
        'première partie qui va remplir la collection de logiciels dans la collection CollLogiciels
        ' ouverture connexion
        ouvrir()

        ' requète qui va chercher les tuples
        Dim requete As String = "select loginAdmin, mdpAdmin from administrateur where loginAdmin = '" + login + "' and mdpAdmin = '" + mdp + "';"

        'Instancier un objet Command
        Dim cmd As New SqlCommand
        With cmd
            'Paramétrer la commande
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With

        'créer l'objet qui reçoit le résultat de la requête
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        'parcourir la table résultat
        While rdr.Read
            ' Remplir la liste Services
            Login_form.Hide()
            Menu_app.Show()

        End While
        'fermer la connexion
        fermer()


    End Sub

    'COLLECTION
    Public Sub remplir_collectiondonateur()
        ouvrir()

        Dim requete As String = "select * from donateur;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        While rdr.Read
            ' instancier un nouvel article
            Dim unbenevole As New CDonnateur(rdr.Item("NomPers"), rdr.Item("PrenomPers"), rdr.Item("AdrPers"), rdr.Item("GSM"))
            'la mettre dans la collection
            CollectionDonateur.Add(unbenevole)
        End While
        fermer()
    End Sub
    Public Sub remplir_collectiondon()
        ouvrir()

        Dim requete As String = "select * from donner;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        While rdr.Read
            ' instancier un nouvel article
            Dim unDon As New CDon(rdr.Item("NumPers"), rdr.Item("DateDon"), rdr.Item("Montant"))
            'la mettre dans la collection
            CollectionDonateur.Add(unDon)
        End While
        fermer()
    End Sub
    Public Sub envoyer_collection_bdd()
        ouvrir()
        'vider les tables donateur et donner
        Dim requete0 As String = "TRUNCATE TABLE donateur;"
        Dim cmd0 As New SqlCommand
        With cmd0

            .Connection = m_Connexion
            .CommandText = requete0
            .CommandType = CommandType.Text
        End With
        cmd0.ExecuteReader()
        fermer()

        ouvrir()
        'vider les tables donateur et donner
        Dim requete As String = "TRUNCATE TABLE donner;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()

        For Each undonnateur In CollectionDonateur
            ouvrir()
            'inserer les valeurs dans la table donateur
            Dim requete1 As String = "insert into donateur(NomPers,PrenomPers,AdrPers,GSM) values ('" + undonnateur.getnomP + "','" + undonnateur.getprenomP + "','" + undonnateur.getadrP + "','" + undonnateur.getGSMP + "');"
            Dim cmd1 As New SqlCommand
            With cmd1

                .Connection = m_Connexion
                .CommandText = requete1
                .CommandType = CommandType.Text
            End With
            cmd1.ExecuteReader()
            fermer()
        Next
        For Each undon In CollectionDon
            ouvrir()
            'inserer les valeurs dans la table don
            Dim requete2 As String = "insert into don values ('" + undon.getNumP + "','" + undon.getdateD + "','" + undon.getMontant + "');"
            Dim cmd2 As New SqlCommand
            With cmd2

                .Connection = m_Connexion
                .CommandText = requete2
                .CommandType = CommandType.Text
            End With
            cmd2.ExecuteReader()
            fermer()
        Next
    End Sub

    'MISSION
    Public Sub remplir_mission()
        ouvrir()
        'Récuperation des données de misison
        Dim requete As String = "select * from mission;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Mission.dgvmissions
                .Rows.Add()
                .Rows(i).Cells(0).Value = rdr(0)
                .Rows(i).Cells(1).Value = rdr(1)
                .Rows(i).Cells(2).Value = rdr(2)
                .Rows(i).Cells(3).Value = rdr(3)
                .Rows(i).Cells(4).Value = rdr(4)
                .Rows(i).Cells(5).Value = rdr(5)
                .Rows(i).Cells(6).Value = rdr(6)

            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub modifier_mission(id, numpays, nom, descript, datedeb, datefin)
        Dim Tpays() As String = Split(numpays, " - ")
        ouvrir()
        'Mis à jour des données de misison
        Dim requete As String = "UPDATE mission SET Npays = '" + Tpays(0) + "', NomM = '" + nom + "', DescM ='" + descript + "', DateDebutM= '" + datedeb + "', DateFinM ='" + datefin + "' WHERE NumM ='" + id + "';"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub remplir_mission_pays()
        ouvrir()
        'Récuperation des données de pays
        Dim requete As String = "select * from pays;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Mission_Ajouter.cbnompays

                Dim test = rdr(0).ToString + " - " + rdr(1).ToString

                .Items.Add(test)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_mission_pays_mod()
        ouvrir()
        'Récuperation des données de pays
        Dim requete As String = "select * from pays;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Mission_Modifier.cbnompays

                Dim test = rdr(0).ToString + " - " + rdr(1).ToString

                .Items.Add(test)
            End With
            i = i + 1
        End While
        fermer()
    End Sub   
    Public Sub remplir_misson_ajouter_membre()
        ouvrir()
        'Récuperation des données de benevole
        Dim requete As String = "select * from benevole;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Mission_Ajouter_membre.cbmembre

                Dim test = rdr(0).ToString + " - " + rdr(1).ToString

                .Items.Add(test)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_mission_ajouter_membre_role()
        ouvrir()
        'Récuperation des données de role
        Dim requete As String = "select * from role;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Mission_Ajouter_membre.cbrole

                Dim test = rdr(0).ToString + " - " + rdr(2).ToString

                .Items.Add(test)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_liste_membre(idmission)
        ouvrir()
        'Récuperation des données du membre de la mission
        Dim requete As String = "SELECT mission.NumM, benevole.NumPers, NomPers From benevole inner join affecter on benevole.NumPers = affecter.NumPers join mission on mission.NumM = affecter.NumM where mission.NumM = '" + idmission + "' ;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        While rdr.Read
            Dim item As ListViewItem
            item = New ListViewItem({rdr(0), rdr(1), rdr(2)})
            With Mission.lvmembre
                .Items.Add(item)
            End With
        End While
        fermer()
    End Sub
    Public Sub supprimer_membre_mission(NumM, NumP)
        ouvrir()
        'Suppression des données du membre de la mission
        Dim requete As String = "DELETE FROM affecter WHERE NumM = '" + NumM + "'AND NumPers = '" + NumP + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub
    Public Sub ajouter_mission(numpays, nom, description, datecrea, datedeb, datefin)
        Dim Tpays() As String = Split(numpays, " - ")
        ouvrir()
        'Insertion des données de misison
        Dim requete As String = "insert into mission(Npays,NomM,DescM,DateCreaM,DateDebutM,DateFinM) values ('" + Tpays(0) + "','" + nom + "','" + description + "','" + datecrea + "','" + datedeb + "','" + datefin + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()

    End Sub
    Public Sub ajouter_membre_mission(numP, numM, codeR)
        Dim Tperso() As String = Split(numP, " - ")
        Dim Trole() As String = Split(codeR, " - ")
        ouvrir()
        'Insertion des données du membre de la misison
        Dim requete As String = "insert into affecter(NumPers,NumM,CodeR) values ('" + Tperso(0) + "','" + numM + "','" + Trole(0) + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub supprimer_mission(NumM As String)
        ouvrir()
        'Suppression des données de misison
        Dim requete As String = "DELETE FROM mission WHERE NumM = '" + NumM + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub
    
    'MEMBRE
    Public Sub remplir_donateur()
        'Remplir les donateur avec la collection Donateur
        Dim i As Integer = 0
        For Each unDonnateur In CollectionDonateur
            With Membre.dgvdonateur
                .Rows.Add()
                .Rows(i).Cells(0).Value = unDonnateur.getnumP()
                .Rows(i).Cells(1).Value = unDonnateur.getnomP()
                .Rows(i).Cells(2).Value = unDonnateur.getprenomP()
                .Rows(i).Cells(3).Value = unDonnateur.getadrP()
                .Rows(i).Cells(4).Value = unDonnateur.getGSMP()
            End With
            i = i + 1
        Next
    End Sub
    Public Sub remplir_benevole()
        ouvrir()
        'Récuperation des données de benevole
        Dim requete As String = "select * from benevole;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Membre.dgvbenevole
                .Rows.Add()
                .Rows(i).Cells(0).Value = rdr(0)
                .Rows(i).Cells(1).Value = rdr(1)
                .Rows(i).Cells(2).Value = rdr(2)
                .Rows(i).Cells(3).Value = rdr(3)
                .Rows(i).Cells(4).Value = rdr(4)
                .Rows(i).Cells(5).Value = rdr(5)
                .Rows(i).Cells(6).Value = rdr(6)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_spe_benevole()
        ouvrir()
        'Récuperation des données de specialite
        Dim requete As String = "select * from specialite;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Membre_Ajouter.cbspe

                Dim test = rdr(0).ToString + " - " + rdr(1).ToString

                .Items.Add(test)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub modifier_benevole(id, nom, prenom, adr, gsm, datefin)

        ouvrir()
        'Mise à jours des données de benevole
        Dim requete As String = "UPDATE benevole SET NomPers = '" + nom + "', PrenomPers = '" + prenom + "', AdrPers ='" + adr + "', GSM = '" + gsm + "', DateSortie ='" + datefin + "' WHERE NumPers ='" + id + "';"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_donateur(nom As String, prenom As String, adresse As String, GSM As String)
        Dim unDonnateur As New CDonnateur(nom, prenom, adresse, GSM)
        'la mettre dans la collection
        CollectionDonateur.Add(unDonnateur)
    End Sub
    Public Sub ajouter_benevole(nom As String, prenom As String, adresse As String, GSM As String, dateentre As String, datesortie As String)
        ouvrir()
        'Insertion des données de benevole
        Dim requete As String = "insert into benevole(NomPers,PrenomPers,AdrPers,GSM,dateEntree,DateSortie) values ('" + nom + "','" + prenom + "','" + adresse + "','" + GSM + "','" + dateentre + "','" + datesortie + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_bene_don(nom As String, prenom As String, adresse As String, GSM As String, dateentre As String, datesortie As String)
        ouvrir()
        'Execution de la procedure stocker
        Dim requete As String = "insert into benevole(NomPers,PrenomPers,AdrPers,GSM,dateEntree,DateSortie) values ('" + nom + "','" + prenom + "','" + adresse + "','" + GSM + "','" + dateentre + "','" + datesortie + "');"
        'Dim requete As String = "EXEC creation_Bene_Don '" + nom + "','" + prenom + "','" + adresse + "','" + GSM + "','" + dateentre + "','" + datesortie + "';"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()

        'Donnateur
        Dim unDonnateur As New CDonnateur(nom, prenom, adresse, GSM)
        'la mettre dans la collection
        CollectionDonateur.Add(unDonnateur)

    End Sub
    Public Sub ajouter_spe_benevole(codespe)
        Dim numP As String = ""
        Dim Tspe() As String = Split(codespe, " - ")
        ouvrir()
        'Récuperation des données de specialite
        Dim requete0 As String = "SELECT TOP 1 NumPers FROM benevole ORDER BY NumPers DESC"
        Dim cmd0 As New SqlCommand
        With cmd0

            .Connection = m_Connexion
            .CommandText = requete0
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd0.ExecuteReader()
        While rdr.Read
            numP = rdr(0)
        End While
        fermer()
        ouvrir()
        'Insertion des données dans avoir
        Dim requete As String = "insert into avoir(NumPers,CodeSpe) values ('" + numP + "','" + Tspe(0) + "');"
        Dim cmd As New SqlCommand
            With cmd

                .Connection = m_Connexion
                .CommandText = requete
                .CommandType = CommandType.Text
            End With
            cmd.ExecuteReader()
            fermer()
    End Sub
    Public Sub supprimer_benevole(NumPers)
        ouvrir()
        'Suppression des données de benevole
        Dim requete As String = "DELETE FROM benevole WHERE NumPers = '" + NumPers + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub

    'DON
    Public Sub remplir_don()
        'Remplir les dons avec la collection Don
        Dim i As Integer = 0
        For Each unDon In CollectionDon
            With Don.dgvdon
                .Rows.Add()
                .Rows(i).Cells(0).Value = unDon.getNumP()
                .Rows(i).Cells(1).Value = unDon.getMontant()
                .Rows(i).Cells(2).Value = unDon.getdateD()
            End With
            i = i + 1
        Next
    End Sub
    Public Sub ajouter_don(donateur, montant, datedon)

        Dim Tdonna() As String = Split(donateur.ToString, " - ")

        Dim unDon As New CDon(Tdonna(0), datedon, montant)
        'la mettre dans la collection
        CollectionDon.Add(unDon)
    End Sub

    'VACCIN
    Public Sub remplir_vaccin()
        ouvrir()
        'Récupération des données de vaccin
        Dim requete As String = "select * from vaccin;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Vaccin.dgvvaccin
                .Rows.Add()
                .Rows(i).Cells(0).Value = rdr(0)
                .Rows(i).Cells(1).Value = rdr(1)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub modifier_vaccin(id, descript)
        ouvrir()
        'Mise à jours des données de vaccin
        Dim requete As String = "UPDATE vaccin SET DescV = '" + descript + "' WHERE NumV ='" + id + "';"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_vaccin(description)
        ouvrir()
        'Insertion des données de vaccin
        Dim requete As String = "insert vaccin (DescV) values ('" + description + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub supprimer_vaccin(DescV As String)
        ouvrir()
        'Suppression des données de vaccin
        Dim requete As String = "DELETE FROM vaccin WHERE DescV LIKE '" + DescV + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub

    'SPECIALITE
    Public Sub remplir_spec()
        ouvrir()
        'Récupération des données de specialite
        Dim requete As String = "select * from specialite;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Specialite.dgvspec
                .Rows.Add()
                .Rows(i).Cells(0).Value = rdr(0)
                .Rows(i).Cells(1).Value = rdr(1)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub modifier_specialite(id, descript)
        ouvrir()
        'Mise à jours des données de specialite
        Dim requete As String = "UPDATE specialite SET LibSpe = '" + descript + "' WHERE CodeSpe ='" + id + "';"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_spec(libelle)
        ouvrir()
        'Insertion des données de specialite
        Dim requete As String = "insert specialite (LibSpe) values ('" + libelle + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub supprimer_specialite(LibSpe As String)
        ouvrir()
        'Suppression des données de specialite
        Dim requete As String = "DELETE FROM specialite WHERE LibSpe LIKE '" + LibSpe + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub

    'PAYS
    Public Sub remplir_pays()
        ouvrir()
        'Récupération des données de pays
        Dim requete As String = "select * from pays;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Pays.dgvpays
                .Rows.Add()
                .Rows(i).Cells(0).Value = rdr(0)
                .Rows(i).Cells(1).Value = rdr(1)
                .Rows(i).Cells(2).Value = rdr(2)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_pays_ajouter_vaccin()
        ouvrir()
        'Récupération des données de vaccin
        Dim requete As String = "select * from vaccin;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Pays_Ajouter_Vaccin.cbvaccin

                Dim test = rdr(0).ToString + " - " + rdr(1).ToString

                .Items.Add(test)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_liste_vaccin(idpays)
        ouvrir()
        'Récupération des données de vaccin
        Dim requete As String = "SELECT pays.Npays, vaccin.NumV,DescV From vaccin inner join necessiter on vaccin.NumV = necessiter.NumV join pays on pays.Npays = necessiter.Npays where pays.Npays = '" + idpays + "' ;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        While rdr.Read
            Dim item As ListViewItem
            item = New ListViewItem({rdr(0), rdr(1), rdr(2)})
            With Pays.lvvaccin
                .Items.Add(item)
            End With
        End While
        fermer()
    End Sub
    Public Sub modifier_pays(id, nom, risque)
        ouvrir()
        'Mise à jours des données de pays
        Dim requete As String = "UPDATE pays SET NomPays = '" + nom + "',  Risque = '" + risque + "' WHERE Npays =" + id + ";"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_pays(nom, risque)
        ouvrir()
        'Insertion des données de pays
        Dim requete As String = "insert pays (NomPays,Risque) values ('" + nom + "','" + risque + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_vaccin_pays(numP, numV)
        Dim Tvaccin() As String = Split(numV, " - ")
        ouvrir()
        'Insertion des données de vaccin dans pays
        Dim requete As String = "insert into necessiter(NumV,Npays) values ('" + Tvaccin(0) + "','" + numP + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub supprimer_pays(NomPays As String)
        ouvrir()

        Dim requete As String = "DELETE FROM pays WHERE NomPays = '" + NomPays + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub
    Public Sub supprimer_vaccin_pays(NPays, NumV)
        ouvrir()
        Dim requete As String = "DELETE FROM necessiter WHERE NumV = '" + NumV + "'AND Npays = '" + NPays + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub

    'ROLE
    Public Sub remplir_role()
        ouvrir()

        Dim requete As String = "select * from role;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Role.dgvrole
                .Rows.Add()
                .Rows(i).Cells(0).Value = rdr(0)
                .Rows(i).Cells(1).Value = rdr(1)
                .Rows(i).Cells(2).Value = rdr(2)
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub remplir_role_spec()
        ouvrir()

        Dim requete As String = "select CodeSpe from specialite;"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        Dim rdr As SqlDataReader = cmd.ExecuteReader()
        Dim i As Integer = 0
        While rdr.Read
            With Role_Ajouter.cbcodespec
                .Items.Add(rdr(0))
            End With
            With Role_Modifier.cbcodespec
                .Items.Add(rdr(0))
            End With
            i = i + 1
        End While
        fermer()
    End Sub
    Public Sub modifier_role(id, descript, codespe)
        ouvrir()

        Dim requete As String = "UPDATE role SET LibR = '" + descript + "',  CodeSpe = " + Str(codespe) + " WHERE CodeR =" + id + ";"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub ajouter_role(codespe, libelle)
        ouvrir()

        Dim requete As String = "insert into role (CodeSpe,LibR) values ('" + codespe + "','" + libelle + "');"
        Dim cmd As New SqlCommand
        With cmd

            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
        End With
        cmd.ExecuteReader()
        fermer()
    End Sub
    Public Sub supprimer_role(LibR As String)
        ouvrir()

        Dim requete As String = "DELETE FROM role WHERE LibR LIKE '" + LibR + "';"
        Dim cmd As New SqlCommand
        With cmd
            .Connection = m_Connexion
            .CommandText = requete
            .CommandType = CommandType.Text
            .ExecuteReader()
        End With
        fermer()
    End Sub

End Class