﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pays_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Pays_Ajouter))
        Me.bajout = New System.Windows.Forms.Button()
        Me.rbtreseleve = New System.Windows.Forms.RadioButton()
        Me.rbeleve = New System.Windows.Forms.RadioButton()
        Me.rbmoyen = New System.Windows.Forms.RadioButton()
        Me.rbfaible = New System.Windows.Forms.RadioButton()
        Me.rbaucun = New System.Windows.Forms.RadioButton()
        Me.tbnom = New System.Windows.Forms.TextBox()
        Me.lrisque = New System.Windows.Forms.Label()
        Me.lnom = New System.Windows.Forms.Label()
        Me.lpaysajout = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(12, 268)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(118, 30)
        Me.bajout.TabIndex = 19
        Me.bajout.Text = "Ajouter"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'rbtreseleve
        '
        Me.rbtreseleve.AutoSize = True
        Me.rbtreseleve.BackColor = System.Drawing.Color.White
        Me.rbtreseleve.Location = New System.Drawing.Point(117, 216)
        Me.rbtreseleve.Name = "rbtreseleve"
        Me.rbtreseleve.Size = New System.Drawing.Size(75, 17)
        Me.rbtreseleve.TabIndex = 18
        Me.rbtreseleve.TabStop = True
        Me.rbtreseleve.Text = "Très élevé"
        Me.rbtreseleve.UseVisualStyleBackColor = False
        '
        'rbeleve
        '
        Me.rbeleve.AutoSize = True
        Me.rbeleve.BackColor = System.Drawing.Color.White
        Me.rbeleve.Location = New System.Drawing.Point(117, 193)
        Me.rbeleve.Name = "rbeleve"
        Me.rbeleve.Size = New System.Drawing.Size(52, 17)
        Me.rbeleve.TabIndex = 17
        Me.rbeleve.TabStop = True
        Me.rbeleve.Text = "Elevé"
        Me.rbeleve.UseVisualStyleBackColor = False
        '
        'rbmoyen
        '
        Me.rbmoyen.AutoSize = True
        Me.rbmoyen.BackColor = System.Drawing.Color.White
        Me.rbmoyen.Location = New System.Drawing.Point(117, 170)
        Me.rbmoyen.Name = "rbmoyen"
        Me.rbmoyen.Size = New System.Drawing.Size(57, 17)
        Me.rbmoyen.TabIndex = 16
        Me.rbmoyen.TabStop = True
        Me.rbmoyen.Text = "Moyen"
        Me.rbmoyen.UseVisualStyleBackColor = False
        '
        'rbfaible
        '
        Me.rbfaible.AutoSize = True
        Me.rbfaible.BackColor = System.Drawing.Color.White
        Me.rbfaible.Location = New System.Drawing.Point(117, 147)
        Me.rbfaible.Name = "rbfaible"
        Me.rbfaible.Size = New System.Drawing.Size(53, 17)
        Me.rbfaible.TabIndex = 15
        Me.rbfaible.TabStop = True
        Me.rbfaible.Text = "Faible"
        Me.rbfaible.UseVisualStyleBackColor = False
        '
        'rbaucun
        '
        Me.rbaucun.AutoSize = True
        Me.rbaucun.BackColor = System.Drawing.Color.White
        Me.rbaucun.Location = New System.Drawing.Point(117, 124)
        Me.rbaucun.Name = "rbaucun"
        Me.rbaucun.Size = New System.Drawing.Size(56, 17)
        Me.rbaucun.TabIndex = 14
        Me.rbaucun.TabStop = True
        Me.rbaucun.Text = "Aucun"
        Me.rbaucun.UseVisualStyleBackColor = False
        '
        'tbnom
        '
        Me.tbnom.Location = New System.Drawing.Point(117, 86)
        Me.tbnom.Name = "tbnom"
        Me.tbnom.Size = New System.Drawing.Size(100, 20)
        Me.tbnom.TabIndex = 13
        '
        'lrisque
        '
        Me.lrisque.AutoSize = True
        Me.lrisque.BackColor = System.Drawing.Color.White
        Me.lrisque.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lrisque.Location = New System.Drawing.Point(12, 124)
        Me.lrisque.Name = "lrisque"
        Me.lrisque.Size = New System.Drawing.Size(60, 17)
        Me.lrisque.TabIndex = 12
        Me.lrisque.Text = "Risque :"
        '
        'lnom
        '
        Me.lnom.AutoSize = True
        Me.lnom.BackColor = System.Drawing.Color.White
        Me.lnom.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnom.Location = New System.Drawing.Point(12, 86)
        Me.lnom.Name = "lnom"
        Me.lnom.Size = New System.Drawing.Size(45, 17)
        Me.lnom.TabIndex = 11
        Me.lnom.Text = "Nom: "
        '
        'lpaysajout
        '
        Me.lpaysajout.AutoSize = True
        Me.lpaysajout.BackColor = System.Drawing.Color.White
        Me.lpaysajout.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lpaysajout.Location = New System.Drawing.Point(12, 9)
        Me.lpaysajout.Name = "lpaysajout"
        Me.lpaysajout.Size = New System.Drawing.Size(205, 31)
        Me.lpaysajout.TabIndex = 10
        Me.lpaysajout.Text = "Ajouter un Pays"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(167, 275)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Pays_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(254, 310)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.rbtreseleve)
        Me.Controls.Add(Me.rbeleve)
        Me.Controls.Add(Me.rbmoyen)
        Me.Controls.Add(Me.rbfaible)
        Me.Controls.Add(Me.rbaucun)
        Me.Controls.Add(Me.tbnom)
        Me.Controls.Add(Me.lrisque)
        Me.Controls.Add(Me.lnom)
        Me.Controls.Add(Me.lpaysajout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Pays_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pays_Ajouter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents rbtreseleve As System.Windows.Forms.RadioButton
    Friend WithEvents rbeleve As System.Windows.Forms.RadioButton
    Friend WithEvents rbmoyen As System.Windows.Forms.RadioButton
    Friend WithEvents rbfaible As System.Windows.Forms.RadioButton
    Friend WithEvents rbaucun As System.Windows.Forms.RadioButton
    Friend WithEvents tbnom As System.Windows.Forms.TextBox
    Friend WithEvents lrisque As System.Windows.Forms.Label
    Friend WithEvents lnom As System.Windows.Forms.Label
    Friend WithEvents lpaysajout As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class
