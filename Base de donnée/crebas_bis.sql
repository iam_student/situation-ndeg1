﻿/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     07/11/2017 16:46:40                          */
/*==============================================================*/


drop table if exists affecter;

drop table if exists avoir;

drop table if exists benevole;

drop table if exists donateur;

drop table if exists donner;

drop table if exists mission;

drop table if exists necessiter;

drop table if exists pays;

drop table if exists role;

drop table if exists specialite;

drop table if exists vaccin;

/*==============================================================*/
/* Table: affecter                                              */
/*==============================================================*/
create table affecter
(
   NumPers              int not null AUTO_INCREMENT,
   NumM                 int not null,
   CodeR                int not null,
   primary key (NumPers, NumM),
   key AK_CODER (CodeR)
);

alter table affecter comment 'affecter';

/*==============================================================*/
/* Table: avoir                                                 */
/*==============================================================*/
create table avoir
(
   NumPers              int not null,
   CodesSpe             int not null,
   primary key (NumPers, CodesSpe)
);

alter table avoir comment 'avoir';

/*==============================================================*/
/* Table: benevole                                              */
/*==============================================================*/
create table benevole
(
   NumPers              int not null AUTO_INCREMENT,
   NomPers              varchar(50),
   AdrPers              varchar(50),
   GSM                  int,
   DateEntree           date not null,
   DateSortie           date,
   primary key (NumPers)
);

alter table benevole comment 'benevole';

/*==============================================================*/
/* Table: donateur                                              */
/*==============================================================*/
create table donateur
(
   NumPers              int not null AUTO_INCREMENT,
   NomPers              varchar(50),
   AdrPers              varchar(50),
   GSM                  int,
   primary key (NumPers)
);

alter table donateur comment 'donateur';

/*==============================================================*/
/* Table: donner                                                */
/*==============================================================*/
create table donner
(
   NumPers              int not null,
   DateDon              date not null,
   Montant              int,
   primary key (NumPers, DateDon)
);

alter table donner comment 'donner';

/*==============================================================*/
/* Table: mission                                               */
/*==============================================================*/
create table mission
(
   NumM                 int not null AUTO_INCREMENT,
   Npays                int not null,
   NomM                 varchar(50),
   DescM                text,
   DateCreaM            date,
   DateDebutM           date,
   DateFinM             date,
   primary key (NumM)
);

alter table mission comment 'mission';

/*==============================================================*/
/* Table: necessiter                                            */
/*==============================================================*/
create table necessiter
(
   NumV                 int not null,
   NPays                int not null,
   primary key (NumV, NPays)
);

alter table necessiter comment 'necessiter';

/*==============================================================*/
/* Table: pays                                                  */
/*==============================================================*/
create table pays
(
   Npays                int not null AUTO_INCREMENT,
   NomPays              varchar(50),
   Risque               text,
   primary key (Npays)
);

alter table pays comment 'pays';

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   CodeR                int not null AUTO_INCREMENT,
   CodesSpe             int not null,
   LibR                 text,
   primary key (CodeR)
);

alter table role comment 'role';

/*==============================================================*/
/* Table: specialite                                            */
/*==============================================================*/
create table specialite
(
   CodesSpe             int not null AUTO_INCREMENT,
   LibSpe               text not null,
   primary key (CodesSpe)
);

alter table specialite comment 'specialite';

/*==============================================================*/
/* Table: vaccin                                                */
/*==============================================================*/
create table vaccin
(
   NumV                 int not null AUTO_INCREMENT,
   DescV                text,
   primary key (NumV)
);

alter table vaccin comment 'vaccin';

alter table affecter add constraint FK_AFFECTER foreign key (NumPers)
      references benevole (NumPers) on delete restrict on update restrict;

alter table affecter add constraint FK_AFFECTER2 foreign key (NumM)
      references mission (NumM) on delete restrict on update restrict;

alter table avoir add constraint FK_AVOIR2 foreign key (NumPers)
      references benevole (NumPers) on delete restrict on update restrict;

alter table avoir add constraint FK_AVOIR3 foreign key (CodesSpe)
      references specialite (CodesSpe) on delete restrict on update restrict;

alter table donner add constraint FK_DONNER foreign key (NumPers)
      references donateur (NumPers) on delete restrict on update restrict;

alter table mission add constraint FK_SEDEROULER foreign key (Npays)
      references pays (Npays) on delete restrict on update restrict;

alter table necessiter add constraint FK_NECESSITER foreign key (NPays)
      references pays (Npays) on delete restrict on update restrict;

alter table necessiter add constraint FK_NECESSITER2 foreign key (NumV)
      references vaccin (NumV) on delete restrict on update restrict;

alter table role add constraint FK_EXIGER foreign key (CodesSpe)
      references specialite (CodesSpe) on delete restrict on update restrict;

alter table role add constraint FK_RELATIONSHIP_11 foreign key (CodeR)
      references affecter (CodeR) on delete restrict on update restrict;

