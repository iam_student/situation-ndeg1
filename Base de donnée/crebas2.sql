/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     01/11/2017 19:25:20                          */
/*==============================================================*/


drop table if exists AFFECTER;

drop table if exists AVOIR2;

drop table if exists BENEVOLE;

drop table if exists DONATEUR;

drop table if exists DONNER;

drop table if exists MISSION;

drop table if exists NECESSITER;

drop table if exists PAYS;

drop table if exists ROLE;

drop table if exists SPECIALITE;

drop table if exists VACCIN;

/*==============================================================*/
/* Table: AFFECTER                                              */
/*==============================================================*/
create table AFFECTER
(
   NUMPERS              int not null,
   NUMM                 int not null,
   CODER                int not null,
   primary key (NUMPERS, NUMM),
   key AK_CODER (CODER)
);

/*==============================================================*/
/* Table: AVOIR2                                                */
/*==============================================================*/
create table AVOIR2
(
   NUMPERS              int not null,
   CODESSPE             int not null,
   primary key (NUMPERS, CODESSPE)
);

/*==============================================================*/
/* Table: BENEVOLE                                              */
/*==============================================================*/
create table BENEVOLE
(
   NUMPERS              int not null,
   NOMPERS              varchar(50),
   ADRPERS              varchar(50),
   GSM                  int,
   DATEENTREE           date not null,
   DATESORTIE           date,
   primary key (NUMPERS)
);

/*==============================================================*/
/* Table: DONATEUR                                              */
/*==============================================================*/
create table DONATEUR
(
   NUMPERS              int not null,
   NOMPERS              varchar(50),
   ADRPERS              varchar(50),
   GSM                  int,
   primary key (NUMPERS)
);

/*==============================================================*/
/* Table: DONNER                                                */
/*==============================================================*/
create table DONNER
(
   NUMPERS              int not null,
   DATEDON              date not null,
   MONTANT              int,
   primary key (NUMPERS, DATEDON)
);

/*==============================================================*/
/* Table: MISSION                                               */
/*==============================================================*/
create table MISSION
(
   NUMM                 int not null,
   NPAYS                int not null,
   NOMM                 varchar(50),
   DESCM                text,
   DATECREAM            date,
   DATEDEBUTM           date,
   DATEFINM             date,
   primary key (NUMM)
);

/*==============================================================*/
/* Table: NECESSITER                                            */
/*==============================================================*/
create table NECESSITER
(
   NUMV                 int not null,
   NPAYS                int not null,
   primary key (NUMV, NPAYS)
);

/*==============================================================*/
/* Table: PAYS                                                  */
/*==============================================================*/
create table PAYS
(
   NPAYS                int not null,
   NOMPAYS              varchar(50),
   RISQUE               text,
   primary key (NPAYS)
);

/*==============================================================*/
/* Table: ROLE                                                  */
/*==============================================================*/
create table ROLE
(
   CODER                int not null,
   CODESSPE             int not null,
   LIBR                 text,
   primary key (CODER)
);

/*==============================================================*/
/* Table: SPECIALITE                                            */
/*==============================================================*/
create table SPECIALITE
(
   CODESSPE             int not null,
   LIBSPE               text not null,
   primary key (CODESSPE)
);

/*==============================================================*/
/* Table: VACCIN                                                */
/*==============================================================*/
create table VACCIN
(
   NUMV                 int not null,
   DESCV                text,
   primary key (NUMV)
);

/*==============================================================*/
/* Contraintes	                                                */
/*==============================================================*/

alter table AFFECTER add constraint FK_AFFECTER foreign key (NUMPERS)
      references BENEVOLE (NUMPERS) on delete restrict on update restrict;

alter table AFFECTER add constraint FK_AFFECTER2 foreign key (NUMM)
      references MISSION (NUMM) on delete restrict on update restrict;

/*-----------------------------------------------------------------------------*/
	  
alter table AVOIR2 add constraint FK_AVOIR2 foreign key (NUMPERS)
      references BENEVOLE (NUMPERS) on delete restrict on update restrict;

alter table AVOIR2 add constraint FK_AVOIR3 foreign key (CODESSPE)
      references SPECIALITE (CODESSPE) on delete restrict on update restrict;

/*-----------------------------------------------------------------------------*/
	  
alter table DONNER add constraint FK_DONNER foreign key (NUMPERS)
      references DONATEUR (NUMPERS) on delete restrict on update restrict;

/*-----------------------------------------------------------------------------*/
	    
alter table MISSION add constraint FK_SEDEROULER foreign key (NPAYS)
      references PAYS (NPAYS) on delete restrict on update restrict;

/*-----------------------------------------------------------------------------*/
	    
alter table NECESSITER add constraint FK_NECESSITER foreign key (NPAYS)
      references PAYS (NPAYS) on delete restrict on update restrict;

alter table NECESSITER add constraint FK_NECESSITER2 foreign key (NUMV)
      references VACCIN (NUMV) on delete restrict on update restrict;

/*----------------------------------------------------------------------------*/
	    
alter table ROLE add constraint FK_EXIGER foreign key (CODESSPE)
      references SPECIALITE (CODESSPE) on delete restrict on update restrict;

alter table ROLE add constraint FK_RELATIONSHIP_11 foreign key (CODER)
      references AFFECTER (CODER) on delete restrict on update restrict;

